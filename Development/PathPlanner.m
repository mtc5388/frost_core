% RUN GPS_TRANSFORM.m FIRST

%% Heading logic
% Create array of row center lines at each end. Find heading of UGV along
% path. If heading SE, find closest row end in that direction and put as
% last waypoint. If heading NW, find closest row in that direction and put
% as last waypoint. (insert long pause at heating location waypoint)

exit_waypoint = RowExit(waypoints)

%% Generate path using new "WaypointGenerator" function

% Current UGV Location
current_lat = 40.708427;
current_lon = -77.953656;
alt = 337.72;

% Goal UGV Location
goal_lat = 40.708777;
goal_lon = -77.953661;

% Run variables through WaypointGenerator function
[waypoints, count, pathlength] = WaypointGenerator(current_lat,current_lon,alt,goal_lat,goal_lon)

WaypointPath = figure;
geoscatter(waypoints(:,1), waypoints(:,2));
geobasemap satellite
hold off

%% Path planner for A1 Test Navigation

start_lat = 40.709029850; 
start_lon = -77.954002951;
alt_start = 342.6733;

P1_lat = 40.7089068690000; 	
P1_lon = -77.9538709560000;

[waypoints,count,pathlen] = WaypointGenerator(start_lat,start_lon,alt_start,P1_lat,P1_lon)

WaypointPath = figure;
geoscatter(waypoints(:,1), waypoints(:,2));
geobasemap satellite
hold off


%% Generate PRM NodeMap
%{
% Replaced PRM function with loading a premade PRM map of tree rows
load('TreeRow_PRM_NodeMap.mat');
PRM_NodeMap = figure;
show(TreeRow_PRM_NodeMap);
hold off
%}


TreeRow_PRM_NodeMap = mobileRobotPRM(TreeRow_Map,2500);
PRM_NodeMap = figure;
show(TreeRow_PRM_NodeMap)
hold off



%% Generate Path

start = [1 1];
goal = [18.5 40];
%goal = [27.85 39.5]; % Narrowest pathway

path = findpath(TreeRow_PRM_NodeMap,start, goal)
PRM_WaypointPathMap = figure;
show(TreeRow_PRM_NodeMap)
hold off

%% Path planner function

% Load PRM Map
load('TreeRow_PRM_NodeMap.mat');

% Current UGV Location
current_lat = 40.708427;
current_lon = -77.953656;
alt = 337.72;

% Goal UGV Location
goal_lat = 40.708777;
goal_lon = -77.953661;

% Plot the points
Geoscatter = figure;
geoscatter(current_lat,current_lon,'red');
hold on
geoscatter(goal_lat,goal_lon,'blue');
geobasemap satellite
hold off

% Origin point
origin = [40.708357 -77.953644 alt];

% Translate to ENU and Rotate
field_heading = 331.2919899401670;
thetaR = field_heading - 360; % Offset angle of point headings to true North (360 deg)
Rot = [cosd(thetaR) -sind(thetaR); sind(thetaR) cosd(thetaR)];
thetaL = 360 - field_heading;
RotRev = [cosd(thetaL) -sind(thetaL); sind(thetaL) cosd(thetaL)];

[start_ENU(:,1), start_ENU(:,2)] = geodetic2enu(current_lat, current_lon, alt, origin(1), origin(2), alt, wgs84Ellipsoid);
[goal_ENU(:,1), goal_ENU(:,2)] = geodetic2enu(goal_lat, goal_lon, alt, origin(1), origin(2), alt, wgs84Ellipsoid);
[~,~,Zup] = geodetic2enu(goal_lat, goal_lon, alt, origin(1), origin(2), alt, wgs84Ellipsoid);

start_ENU_Rot = Rot * start_ENU';
goal_ENU_Rot = Rot * goal_ENU';

points = figure;
plot(start_ENU_Rot(1),start_ENU_Rot(2),'.r');
hold on
plot(goal_ENU_Rot(1),goal_ENU_Rot(2),'.b');
hold off


% Pass points into PRM map to make waypoints
WP = findpath(TreeRow_PRM_NodeMap,start_ENU_Rot', goal_ENU_Rot');
PRM_WaypointPathMap = figure;
show(TreeRow_PRM_NodeMap)
hold off


% Rotate and convert waypoints back to GPS coords
waypoints_Rot = RotRev * WP';
[waypoints_GPS(:,1), waypoints_GPS(:,2)] = enu2geodetic(waypoints_Rot(1,:), waypoints_Rot(2,:), Zup, origin(1), origin(2), alt, wgs84Ellipsoid);



%% Interpolate Path for More Waypoints
%{
path_waypts = [];
interp_num = 10; % 10 points generated between each waypoint

for b = 1:height(path);
    path_points = interp1([path(b,1) path(b,2)], [path(b+1,1) path(b+1,2)])
    path_waypts = vertcat(path_waypts, path_points)
end
%}

%% Astar Planner- TO BE POTENTIALLY REMOVED
%{
planner = plannerAStarGrid(TreeRow_Map);

start = [2 2];
goal = [450 400];

plan(planner,start,goal);
show(planner)
%}