function [exit_waypoint] = RowExit(waypoints)
%

% Load row centerline coordinates
load('row_centers_lat.mat');
load('row_centers_lon.mat');
load('row_centerinterp_lon.mat');
load('row_centerinterp_lat.mat');

% Heading parameters for row exit, if UGV heading ~= heading_SE, UGV is
% heading NW
heading_SE = [61 241]; % Deg CW from True North

% Count number of waypoints
count = size(waypoints,1);

% Find UGV's heading at end of waypoint mission
UGV_heading = azimuth(waypoints(count-1,1),waypoints(count-1,2),waypoints(count,1),waypoints(count,2),wgs84Ellipsoid);

% Test heading to find direction, extract closest row center point to exit
% row end
exit_waypoint = [];
if (UGV_heading > heading_SE(1)) && (UGV_heading < heading_SE(2))
    disp('UGV is heading SE')


    dist = distance(waypoints(count,1),waypoints(count,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
    [~,col] = find(dist == min(min(dist)));
    
    exit_waypoint(1) = row_centers_lat(2,col);
    exit_waypoint(2) = row_centers_lon(2,col);
    

else
    disp('UGV is heading NW')
    

    dist = distance(waypoints(count,1),waypoints(count,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
    [~,col] = find(dist == min(min(dist)));

    exit_waypoint(1) = row_centers_lat(1,col);
    exit_waypoint(2) = row_centers_lon(1,col);

end    


end