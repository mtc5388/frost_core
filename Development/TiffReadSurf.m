function [tempvalues] = TiffProcessor(tiffaddress)
% Read Tiff, get temperature, make surf map
% To convert pixel values to degrees Celsius, multiply the entire image by
% 0.01 and then subtract 273.15.


% FIX ORIENTATION OF PIC!

tiff = Tiff(tiffaddress,'r');
pixelvalues = read(tiff);
tempvalues = pixelvalues * 0.01 - 273.15;

surf(tempvalues);
colormap('cool');
title('Pixel Temperature Map');
xlabel('Width [pixels]');
ylabel('Depth [pixels]');
zlabel(['Temperature [' char(176) 'C]']);
colorbar eastoutside;
end

