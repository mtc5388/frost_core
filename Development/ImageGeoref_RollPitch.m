function [image_georef_lat,image_georef_lon,offset_UAS_center] = ImageGeoref_RollPitch(UAS_altitude,UAS_GPS_position, UAS_pitch, UAS_roll, UAS_heading)

%{
Imported variables from UAS (or assumed for testing)
UAS_altitude: AGL altitude in meters
UAS_GPS_position: GPS position of drone when image is taken
UAS_heading: [Deg] Positive CW from North
%}

% Variables for calculation (from FLIR Duo R specsheet)
cam_width_angle = 57; % Degrees
cam_depth_angle = 44; % Degrees
cam_width_pixels = 160; % Pixels
cam_depth_pixels = 120; % Pixels

% Load pitch and roll variables
pitch = UAS_pitch;
roll = UAS_roll;

% Georeferenced matrices equal to dimensions of FLIR Duo R pixel
% resolution. Two arrays, for latitude and longitude
image_georef_lat = zeros(cam_depth_pixels,cam_width_pixels);
image_georef_lon = zeros(cam_depth_pixels,cam_width_pixels);

% Calculate offset angles for corners and center including roll and pitch
x_offset_left = tand(cam_width_angle/2 - (cam_width_angle/cam_width_pixels)/2 - roll) * UAS_altitude;
x_offset_right = tand(cam_width_angle/2 - (cam_width_angle/cam_width_pixels)/2 + roll) * UAS_altitude;
y_offset_front = tand(cam_depth_angle/2 - (cam_depth_angle/cam_depth_pixels)/2 - pitch) * UAS_altitude;
y_offset_back = tand(cam_depth_angle/2 - (cam_depth_angle/cam_depth_pixels)/2 + pitch) * UAS_altitude;

x_offset_cent = tand(abs(roll)) * UAS_altitude;
y_offset_cent = tand(abs(pitch)) * UAS_altitude;

% Find distance offset for corners using offset angles (CW where #1 is top right of image)
dist_offset_1 = sqrt(x_offset_right^2 + y_offset_front^2)/1000;
dist_offset_1deg = km2deg(dist_offset_1,'earth');
dist_offset_2 = sqrt(x_offset_right^2 + y_offset_back^2)/1000;
dist_offset_2deg = km2deg(dist_offset_2,'earth');
dist_offset_3 = sqrt(x_offset_left^2 + y_offset_back^2)/1000;
dist_offset_3deg = km2deg(dist_offset_3,'earth');
dist_offset_4 = sqrt(x_offset_left^2 + y_offset_front^2)/1000;
dist_offset_4deg = km2deg(dist_offset_4,'earth');
dist_offset_cent = sqrt(x_offset_cent^2 + y_offset_cent^2)/1000;
dist_offset_centdeg = km2deg(dist_offset_cent,'earth');


% Calculate azimuth angle for each corner offset (CW where #1 is top right)
az_1 = UAS_heading - (270 + tand(y_offset_front/x_offset_right) + 45);
az_2 = UAS_heading - (180 + tand(x_offset_right/y_offset_back) + 45);
az_3 = UAS_heading - (90 + tand(y_offset_back/x_offset_left) + 45);
az_4 = UAS_heading - (tand(x_offset_left/y_offset_front) + 45);

% Find center azimuth by testing what quadrant and find azimuth angle (CW where if #1 is top right)
if (pitch <= 0) && (roll >= 0)
    az_cent = UAS_heading - 270 - tand(y_offset_cent/x_offset_cent);
elseif (pitch >= 0) && (roll >= 0)
    az_cent = UAS_heading - 180 - tand(x_offset_cent/y_offset_cent);
elseif (pitch >= 0) && (roll <= 0)
    az_cent = UAS_heading - 90 - tand(y_offset_cent/x_offset_cent);
elseif (pitch <= 0) && (roll <= 0)
    az_cent = UAS_heading - tand(x_offset_cent/y_offset_cent);
end 

% Find offset center coordinates accounting for roll and pitch using Reckon
[offset_UAS_center(1,1), offset_UAS_center(1,2)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2),dist_offset_centdeg,az_cent);

% Find corner coordinates using offset center coords and Reckon and write to Image GPS Matrices
[image_georef_lat(1,cam_width_pixels), image_georef_lon(1,cam_width_pixels)] = reckon(offset_UAS_center(1,1),offset_UAS_center(1,2), dist_offset_1deg, az_1);
[image_georef_lat(cam_depth_pixels,cam_width_pixels), image_georef_lon(cam_depth_pixels,cam_width_pixels)] = reckon(offset_UAS_center(1,1),offset_UAS_center(1,2), dist_offset_2deg, az_2);
[image_georef_lat(cam_depth_pixels,1), image_georef_lon(cam_depth_pixels,1)] = reckon(offset_UAS_center(1,1),offset_UAS_center(1,2), dist_offset_3deg, az_3);
[image_georef_lat(1,1), image_georef_lon(1,1)] = reckon(offset_UAS_center(1,1),offset_UAS_center(1,2), dist_offset_4deg, az_4);


% Interpolate all pixel GPS coordinates between corners
image_georef_lat(1,:) = linspace(image_georef_lat(1,1), image_georef_lat(1,cam_width_pixels),cam_width_pixels);
image_georef_lat(cam_depth_pixels,:) = linspace(image_georef_lat(cam_depth_pixels,1), image_georef_lat(cam_depth_pixels,cam_width_pixels),cam_width_pixels);
for g = 1:cam_width_pixels
    image_georef_lat(:,g) = linspace(image_georef_lat(1,g), image_georef_lat(cam_depth_pixels,g),cam_depth_pixels);
end
image_georef_lon(1,:) = linspace(image_georef_lon(1,1), image_georef_lon(1,cam_width_pixels),cam_width_pixels);
image_georef_lon(cam_depth_pixels,:) = linspace(image_georef_lon(cam_depth_pixels,1), image_georef_lon(cam_depth_pixels,cam_width_pixels),cam_width_pixels);
for g = 1:cam_width_pixels
    image_georef_lon(:,g) = linspace(image_georef_lon(1,g), image_georef_lon(cam_depth_pixels,g),cam_depth_pixels);
end


end

