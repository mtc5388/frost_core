#!/usr/bin/env python
"""
Flight data readout for NSF Matrice 100 drone. "sdk.launch" must be launched first.
Will import:
/gps_position (lat, lon, alt)
/attitude (orientation quaternion in ENU frame)

"""
import rospy
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import QuaternionStamped
from sensor_msgs.msg import Imu
from sensor_msgs.msg import NavSatFix
import tf

def attitude_callback(data):

	quaternion = (data.quaternion.x,data.quaternion.y,data.quaternion.z,data.quaternion.w)

	euler = tf.transformations.euler_from_quaternion(quaternion)

	roll= euler[0] * (180/3.141592)
	pitch = euler[1] * (180/3.141592)
	yaw = euler[2] * (180/3.141592)-90

	rospy.loginfo("yaw_imu %s:",yaw)

def gps_callback(data):

	lat = data.latitude
	lon = data.longitude
	alt = data.altitude

	rospy.loginfo("lat, lon %s,%s:", lat, lon)

def altitude_callback(data):

	alt = data

	rospy.loginfo("altitude %s:", alt)

def listener():

    rospy.init_node('drone_orientation_data', anonymous=True)
    rospy.Subscriber("/dji_sdk/attitude",QuaternionStamped, attitude_callback)
    rospy.Subscriber("/dji_sdk/gps_position",NavSatFix, gps_callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
