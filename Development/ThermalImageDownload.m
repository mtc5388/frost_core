%{
Maxfield Canto, mtc5388
2/7/21
Description: 
MATLAB script to change to the Networking directory and run
two scripts for FLIR image capture. 
The first script "image_capturescript" will SSH into the
Odroid computer and run the camera_trigger.py file onboard. This will send
a gpio output to the arduino which will trigger the FLIR camera using a PWM
signal. 
The next script "image_downloadscript" will access the Odroid's
filesystem and copy over the newest .tiff formatted image into base
station's Networking directory.
Base station and odroid must be connected to the same wireless network for
scripts to run. Ensure the IP address for the Odroid is correct in all
scripts.
Odroid IP on Netgear Router: 172.16.0.2
NOT THIS ONE cd catkin_ws/src/Onboard-SDK-ROS/dji_sdk
cd ~/catkin_ws/src/Onboard-SDK-ROS-master/

export ROS_IP=172.16.0.2
export ROS_HOSTNAME=localhost
export ROS_MASTER_URI=http://localhost:11311

echo $ROS_IP
$echo $ROS_MASTER_URI
%}

%% Trigger Camera, Take Image
system('cd ~/Documents/frost_core/Networking ; ./image_capturescript');
pause(10); % Pause so camera can reconnect to computer after image capture
%% Download Newest Image
system('cd ~/Documents/frost_core/Networking ; ./image_downloadscript');



