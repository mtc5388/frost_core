% Code to import and analyze the deviance of the UGV compared to the
% planned waypoint path.
% 

%% Import Waypoint Path Plan, Pool Priority Locations, and UGV Nav Data
% Path coords from csv file. Exported from U-center table view.
clc, clear

load('waypoint_paths_multi5_FieldTesting.mat');

load('priority_waypoints.mat');
wp_priority = sortrows(priority_waypoints,4,'descend');

pool_number = input("Enter the pool location number (1-5)\n");
[path_number,~] = find(wp_priority == pool_number);


UGV_path = readmatrix("Pool" + pool_number + "_Log.csv",'Range','A:D');


%% Interpolate points on path based on data and find closest points to waypoints and analyze error

interp_num = floor((size(UGV_path,1) - size(wp_priority,1))/size(waypoint_paths(path_number).gps_coords,1));

idx_1 = 1; 
idx_2 = interp_num;
interp_path = [];
for j = 1:size(waypoint_paths(path_number).gps_coords,1) - 1

    interp_path(idx_1:idx_2, 1) = linspace(waypoint_paths(path_number).gps_coords(j,1),waypoint_paths(path_number).gps_coords(j+1,1),interp_num)';
    interp_path(idx_1:idx_2, 2) = linspace(waypoint_paths(path_number).gps_coords(j,2),waypoint_paths(path_number).gps_coords(j+1,2),interp_num)';

    idx_1 = idx_1 + interp_num;
    idx_2 = idx_2 + interp_num;
end

for i = 1:size(interp_path,1)
    
    dev_lat(i,1) = min(abs(UGV_path(:,2) - interp_path(i,1)));
    dev_lon(i,1) = min(abs(UGV_path(:,3) - interp_path(i,2)));


end

dev_meter_lat = deg2km(dev_lat,'earth')*1000;
dev_meter_lon = deg2km(dev_lon,'earth')*1000;

avg_dev_lat = mean(dev_lat);
avg_dev_lon = mean(dev_lon);

% Statistical results (all in Meters)
Lat_MAE = deg2km(avg_dev_lat,'earth')*1000;
Lon_MAE = deg2km(avg_dev_lon,'earth')*1000;

Lat_RMSE = sqrt(sumsqr(dev_meter_lat)/size(interp_path,1));
Lon_RMSE = sqrt(sumsqr(dev_meter_lon)/size(interp_path,1));

Lat_StDev = std(dev_meter_lat);
Lon_StDev = std(dev_meter_lon);

Lat_RSS = sumsqr(dev_meter_lat);
Lat_TSS = sumsqr(dev_meter_lat - mean(dev_meter_lat));
Lat_R_Sqr = 1 - (Lat_RSS/Lat_TSS);


final_target_lat = deg2km(min(abs(UGV_path(:,2) - waypoint_paths(path_number).gps_coords(end,1))),'earth')*1000;
final_target_lon = deg2km(min(abs(UGV_path(:,3) - waypoint_paths(path_number).gps_coords(end,2))),'earth')*1000;

%% Path Plotter
load('row_points_lat.mat');
load('row_points_lon.mat');

plot = figure;
%plot.WindowState = 'maximized';
gx = geoaxes;
geoplot(interp_path(:,1),interp_path(:,2),'white','LineWidth',4) % Planned UGV path
hold on
%geoplot(UGV_path(:,2),UGV_path(:,3),':blue','LineWidth',4) % Actual UGV path
hold on
geoscatter(interp_path(end,1),interp_path(end,2),300,'red','filled');
hold on
geoscatter(interp_path(1,1),interp_path(1,2),300,'green','filled');
hold on

for b = 1:8
    geoplot(row_points_lat([6 160],b), row_points_lon([6 160],b), '-black','LineWidth',3)
    hold on
end
hold off
geotickformat -dd
% Commands to remove axes
%title('Path 1','FontSize',16)
gx.Grid = 'off';
gx.Scalebar.Visible = 'off';
gx.LatitudeAxis.Visible = 'off';
gx.LongitudeAxis.Visible = 'off';
geobasemap streets-dark



%% ENU Path Plotter

% alt = 338.3908; %meters HAE
% origin_GPS = [40.708442, -77.954042, alt];
% heading = 331; % deg from True North (clockwise)
% 
% % Convert interp_path and UGV_path to ENU
% for k = 1:size(interp_path,1)
%     [interp_path_ENU(k,1), interp_path_ENU(k,2)] = geodetic2enu(interp_path(k,1),interp_path(k,2),alt,origin_GPS(1),origin_GPS(2),alt,wgs84Ellipsoid);
% end
% for l = 1:size(UGV_path,1)
%     [UGV_path_ENU(l,1), UGV_path_ENU(l,2)] = geodetic2enu(UGV_path(l,2),UGV_path(l,3),alt,origin_GPS(1),origin_GPS(2),alt,wgs84Ellipsoid);
% end

%% Plot ENU Path

% 
% plot(interp_path_ENU(:,1),interp_path_ENU(:,2),'black')
% hold on
% plot(UGV_path_ENU(:,1), UGV_path_ENU(:,2),'--blue')
% hold off
% set(gca,'color',[0.5 0.5 0.5])










