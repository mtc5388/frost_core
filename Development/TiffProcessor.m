function [tempvalues, min_temp, max_temp] = TiffProcessor(tiffaddress)
% Read Tiff, get temperature, make surf map
% To convert pixel values to degrees Celsius, multiply the entire image by
% 0.01 and then subtract 273.15.


%tiff = Tiff(tiffaddress,'r');
%pixelvalues = read(tiff);

pixelvalues = double(imread(tiffaddress));
%pixelvaluesrot = double(imrotate(flip(pixelvalues,2),180));
tempvalues = (pixelvalues * 0.01) - 273.15;

min_temp = min(min(tempvalues));
max_temp = max(max(tempvalues));

% surf(tempvalues);
% colormap('cool');
% title('Pixel Temperature Map');
% xlabel('Width [pixels]');
% ylabel('Depth [pixels]');
% zlabel(['Temperature [' char(176) 'C]']);
% h = colorbar("eastoutside");
% ylabel(h,'Degrees C')
end

