

%% Find random goal points, export the points and path navigation length
% MUST PRELOAD "TreeRow_PRM_NodeMap.mat" SO FUNCTION CAN RUN
load('TreeRow_BinaryOccMatrix.mat');
load('TreeRow_PRM_NodeMap.mat');
results = [];
% Fixed starting position middle-East of the rows
start_ENU_X = 34.5;
start_ENU_Y = 40;

tic;
for i = 1:100
    try
        idx = find(TreeRow_MapMatrix == 0);
        randsamp = randi(length(idx),1,1);
        index = idx(randsamp);
        [row,col] = ind2sub(size(TreeRow_MapMatrix),index);
        goal_ENU_X = col/10;
        goal_ENU_Y = 75 - row/10;
    
        [results(i,[1 2]) results(i,3)] = WaypointGenerator_PathTests(start_ENU_X, start_ENU_Y,goal_ENU_X,goal_ENU_Y,TreeRow_PRM_NodeMap);
    catch
        fprintf('Goal error\n')
        continue
    end
end
toc

%% Plot results

geoscatter(results(:,1), results(:,2))
geobasemap satellite





%{
%% Pass through waypoint function
start_ENU_X = 34.5;
start_ENU_Y = 40;

goal_GPS = WaypointGenerator_PathTests(start_ENU_X, start_ENU_Y,goal_ENU_X,goal_ENU_Y);

%geoplot(waypoints(:,1),waypoints(:,2))
%geobasemap satellite;
%}