function [image_georef_lat,image_georef_lon,pixel_width,pixel_height] = ImageGeoref(UAS_altitude,UAS_GPS_position, UAS_heading)

%{
Imported variables from UAS (or assumed for testing)
UAS_altitude: AGL altitude in meters
UAS_GPS_position: GPS position of drone when image is taken
UAS_heading: [Deg] Positive CW from North
%}

% Variables for calculation (from FLIR Duo R specsheet)
cam_width_angle = 57; % Degrees
cam_depth_angle = 44; % Degrees
cam_width_pixels = 160; % Pixels
cam_depth_pixels = 120; % Pixels

% Georeferenced matrices equal to dimensions of FLIR Duo R pixel
% resolution. Two arrays, for latitude and longitude
image_georef_lat = zeros(cam_depth_pixels,cam_width_pixels);
image_georef_lon = zeros(cam_depth_pixels,cam_width_pixels);

% Calculate offset distance for corners
x_offset = tand(cam_width_angle/2 - (cam_width_angle/cam_width_pixels)/2) * UAS_altitude; % Horizontal offset in meters
y_offset = tand(cam_depth_angle/2 - (cam_depth_angle/cam_depth_pixels)/2) * UAS_altitude; % Vertical offset in meters
distance_offset = sqrt(x_offset^2 + y_offset^2)/1000; % Diagononal (Hypotenuse) distance in meters
distance_offset_deg =  km2deg(distance_offset,'earth'); % Offset distance in Earth degrees

% Calculate azimuth angle for each corner offset (CW where #1 is top right)
az_1 = UAS_heading - (270 + tand(y_offset/x_offset) + 45);
az_2 = UAS_heading - (180 + tand(x_offset/y_offset) + 45);
az_3 = UAS_heading - (90 + tand(y_offset/x_offset) + 45);
az_4 = UAS_heading - (tand(x_offset/y_offset) + 45);

% Find corner coordinates using Reckon and write to Image GPS Matrices
image_georef_lat(cam_depth_pixels/2,cam_width_pixels/2) = UAS_GPS_position(1,1);
image_georef_lon(cam_depth_pixels/2,cam_width_pixels/2) = UAS_GPS_position(1,2);
[image_georef_lat(1,cam_width_pixels), image_georef_lon(1,cam_width_pixels)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2), distance_offset_deg, az_1);
[image_georef_lat(cam_depth_pixels,cam_width_pixels), image_georef_lon(cam_depth_pixels,cam_width_pixels)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2), distance_offset_deg, az_2);
[image_georef_lat(cam_depth_pixels,1), image_georef_lon(cam_depth_pixels,1)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2), distance_offset_deg, az_3);
[image_georef_lat(1,1), image_georef_lon(1,1)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2), distance_offset_deg, az_4);

% Interpolate all pixel GPS coordinates between corners
image_georef_lat(1,:) = linspace(image_georef_lat(1,1), image_georef_lat(1,cam_width_pixels),cam_width_pixels);
image_georef_lat(cam_depth_pixels,:) = linspace(image_georef_lat(cam_depth_pixels,1), image_georef_lat(cam_depth_pixels,cam_width_pixels),cam_width_pixels);
for g = 1:cam_width_pixels
    image_georef_lat(:,g) = linspace(image_georef_lat(1,g), image_georef_lat(cam_depth_pixels,g),cam_depth_pixels);
end
image_georef_lon(1,:) = linspace(image_georef_lon(1,1), image_georef_lon(1,cam_width_pixels),cam_width_pixels);
image_georef_lon(cam_depth_pixels,:) = linspace(image_georef_lon(cam_depth_pixels,1), image_georef_lon(cam_depth_pixels,cam_width_pixels),cam_width_pixels);
for g = 1:cam_width_pixels
    image_georef_lon(:,g) = linspace(image_georef_lon(1,g), image_georef_lon(cam_depth_pixels,g),cam_depth_pixels);
end

pixel_width = x_offset/80;
pixel_height = y_offset/60;

end

