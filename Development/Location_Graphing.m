% Translate results from UAV testing into ENU coordinates for graphing
clc, clear

load('UAV_Locations.mat');
wgs84 = wgs84Ellipsoid;
alt = 338.3908; %meters HAE
origin_GPS = [40.708442, -77.954042, alt];


for i = 1:size(UAV_Locations,1)
    [Trial2(i,1), Trial2(i,2)] = geodetic2enu(UAV_Locations(i,2), UAV_Locations(i,3), alt, origin_GPS(1), origin_GPS(2), alt, wgs84Ellipsoid);
    [Trial1(i,1), Trial1(i,2)] = geodetic2enu(UAV_Locations(i,4), UAV_Locations(i,5), alt, origin_GPS(1), origin_GPS(2), alt, wgs84Ellipsoid);
    [Known(i,1), Known(i,2)] = geodetic2enu(UAV_Locations(i,6), UAV_Locations(i,7), alt, origin_GPS(1), origin_GPS(2), alt, wgs84Ellipsoid);

end

%% Plot ENU coords
scatter(Trial1(:,1),Trial1(:,2),'g')
hold on
scatter(Trial2(:,1),Trial2(:,2),'b')
hold on
scatter(Known(:,1),Known(:,2),'r')
hold on