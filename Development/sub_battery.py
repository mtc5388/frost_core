#!/usr/bin/env python
"""
Battery state readout for NSF Matrice 100 drone. "sdk.launch" must be launched first
"""
import rospy
from sensor_msgs.msg import BatteryState
import numpy as np

def battery(topic):
	rospy.init_node('battery')
	rospy.Subscriber(topic+"/battery_state", BatteryState, battery_callback)
	rospy.spin()

def battery_callback(msg):
    rospy.loginfo("Battery is at %s", msg.percentage)

if __name__ == '__main__':
	battery("dji_sdk")
