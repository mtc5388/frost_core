
%% Upload Variables

% Load PRM Map
load('TreeRow_PRM_NodeMap.mat');

% Current UGV Location
current_lat = 40.708427;
current_lon = -77.953656;
alt = 337.72;

% Goal UGV Location
goal_lat = 40.708777;
goal_lon = -77.953661;

% Plot the points
Geoscatter = figure;
geoscatter(current_lat,current_lon,'red');
hold on
geoscatter(goal_lat,goal_lon,'blue');
geobasemap satellite
hold off

% Origin point
origin = [40.708357 -77.953644 alt];

% Translate to ENU and Rotate
field_heading = 331.2919899401670;
thetaR = field_heading - 360; % Offset angle of point headings to true North (360 deg)
Rot = [cosd(thetaR) -sind(thetaR); sind(thetaR) cosd(thetaR)];
thetaL = 360 - field_heading;
RotRev = [cosd(thetaL) -sind(thetaL); sind(thetaL) cosd(thetaL)];

[start_ENU(:,1), start_ENU(:,2)] = geodetic2enu(current_lat, current_lon, alt, origin(1), origin(2), alt, wgs84Ellipsoid);
[goal_ENU(:,1), goal_ENU(:,2)] = geodetic2enu(goal_lat, goal_lon, alt, origin(1), origin(2), alt, wgs84Ellipsoid);
[~,~,Zup] = geodetic2enu(goal_lat, goal_lon, alt, origin(1), origin(2), alt, wgs84Ellipsoid);

start_ENU_Rot = Rot * start_ENU';
goal_ENU_Rot = Rot * goal_ENU';

points = figure;
plot(start_ENU_Rot(1),start_ENU_Rot(2),'.r');
hold on
plot(goal_ENU_Rot(1),goal_ENU_Rot(2),'.b');
hold off

%% Create RRT State Space

% Create state space
ss = stateSpaceSE2;
% Create state validator
sv = validatorOccupancyMap(ss);
% Populate state validator constraints
sv.Map = TreeRow_PRM_NodeMap.Map;
sv.ValidationDistance = 0.01;
ss.StateBounds = [TreeRow_PRM_NodeMap.Map.XWorldLimits;TreeRow_PRM_NodeMap.Map.YWorldLimits; [-pi pi]];

% Run RRT planner and set connection distance
planner = plannerRRT(ss,sv);
planner.MaxConnectionDistance = 0.8;
planner.MaxNumTreeNodes = 500000;

% Rotating start and goal coords and adding fake pose value
start = start_ENU_Rot';
goal = goal_ENU_Rot';
start(1,3) = -pi/2;
goal(1,3) = 0;

% NOTE: RRT Planner needs coordinates as [x,y,pose] for input
rng(100,'twister');
[pthObj,solnInfo] = plan(planner,start,goal);

% Show resultant tree expansions and final path on map
show(TreeRow_PRM_NodeMap.Map)
hold on
plot(solnInfo.TreeData(:,1),solnInfo.TreeData(:,2),'.-'); % tree expansion
plot(pthObj.States(:,1),pthObj.States(:,2),'r-','LineWidth',2) % draw path

