%% Maxfield Canto, 12/1/2021
% Adapted from "DJI-ROS-Simulink" MATLAB add-on. Instructions listed in the
% "DJIROSWaypointFollowerExample.mlx file within the add-on.


%% Incase of errors, shutdown service
rosshutdown

%% Initialize parameters

OdroidIP = '172.16.0.2';
OdroidURI = append('http://',OdroidIP,':11311');

%% Initialize ROS connection to onboard Odroid computer
% NOTE- dji_sdk_node.launch must be running on Odroid already
% If this doesn't work make sure /etc/hosts on Odroid is correct for both
% computers
rosinit(OdroidURI)

% Ensure that dji_ros_sdk node is running on Odroid
rosnode list

%% Set ROS URI to Odroid
OdroidURI = append('http://',OdroidIP,':11311'); 
setenv('ROS_MASTER_URI',OdroidURI)
setenv('ROS_IP',OdroidIP)

%% Set up custom messages
% NOTE: Current MATLAB working directory must be /frost_core/Networking/DJI-ROS-Simulink

%exampleDJIPackages = fullfile(pwd,'dji_task_control_package');
rosgenmsg('~/Documents/frost_core/Networking/DJI-ROS-Simulink/dji_task_control_package');

%% Open Simulink model
open_system('ROS_DJI_waypoint_follower');

%% Upload waypoints using file
exampleHelperPublish_WayPointsTEST