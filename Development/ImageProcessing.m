
%% Camera calibration testing
clc, clear
load('cameraParams.mat');

values = imread('Flight1.tiff');

fixed_tempvalues = undistortImage(values,cameraParams,'OutputView','same');

%% Write to Tiff

imwrite(fixed_tempvalues, '4_22_Flight1_Corrected.tiff','Compression','none');


%% Read Tiff, get temperature, make surf map
% To convert pixel values to degrees Celsius, multiply the entire image by
% 0.01 and then subtract 273.15.

%[tempvalues min_temp max_temp] = TiffProcessor('20220301_142454_214.tiff');

[tempvalues, min_temp, max_temp] = TiffProcessor('4_22_Flight1_Corrected.tiff');

%tempvalues = fixed_tempvalues;

[row, col] = find(tempvalues == min_temp);

imshow(tempvalues,[min_temp, max_temp]);

%{

surf(tempvalues);
colormap('cool');
title('Pixel Temperature Map');
xlabel('Width [pixels]');
ylabel('Depth [pixels]');
zlabel(['Temperature [' char(176) 'C]']);
colorbar eastoutside;

%pointCloud(tempvalues)
%}

%% Correction for Pool Temp using GCP

pool_temp = input('Input measured pool temp (deg C)\n');

diff = abs(min(tempvalues,[],'all') - pool_temp);
tempvalues_corr = tempvalues - diff;

mean_temp = mean(tempvalues_corr,'all');

temp_val_corr = figure;
surf(tempvalues_corr);
colormap('cool');
title('Pixel Temperature Corrected');
xlabel('Width [pixels]');
ylabel('Depth [pixels]');
zlabel(['Temperature [' char(176) 'C]']);
h = colorbar("eastoutside");
ylabel(h,'Degrees C')
hold off

%% Image Georeferencing- Using Function

% ImageGeoref function inputs: altitude, [UAS GPS location], heading (CW
% from True North)
%[image_georef_lat, image_georef_lon] = ImageGeoref(75, [40.708793 -77.953632], 331.292);

%Example calculation for Flight Test 3/16/22- Perp 2- 226ft
%[image_georef_lat, image_georef_lon,pix_width,pix_height] = ImageGeoref(65.8996276855, [40.7088725362 -77.9536727751], 238.518651251);

% Calculation for FT 4/11/22- Flight 3
[image_georef_lat, image_georef_lon,pix_width,pix_height] = ImageGeoref(63.5129089355, [40.7087756635 -77.9537119247], 245.066684565);

% 4/11/22- Flight 5
%[image_georef_lat, image_georef_lon,pix_width,pix_height] = ImageGeoref(66.2619628906, [40.7087895049 -77.9536927969], 245.56223468);

% 4/11/22- Flight 2
%[image_georef_lat, image_georef_lon,pix_width,pix_height] = ImageGeoref(65.8274841309, [40.7087669018 -77.9536693923], 239.422182388);

%% Image Georeferencing FLIGHTS 4/11/22
% Roll and pitch follow +CCW reference frame. Heading is +CW.

% 4/11/22- Flight 1, Pool 2 [46 100]
% [image_georef_lat, image_georef_lon] = ImageGeoref(65.8996276855,[40.7088725362 -77.9536727751], 238.518651251);
% row_idx = 46; col_idx = 100;

% 4/11/22- Flight 2, Pool 5 [51 10]
% [image_georef_lat, image_georef_lon] = ImageGeoref(65.8274841309, [40.7087669018 -77.9536693923], 239.422182388); 
% row_idx = 51; col_idx = 10;

% 4/11/22- Flight 3, Pool 3 [58 68]
% [image_georef_lat, image_georef_lon] = ImageGeoref(63.5129089355, [40.7087756635 -77.9537119247], 245.049310789);
% row_idx = 58; col_idx = 68;

% 4/11/22- Flight 4, Pool 1 [19 107]
% [image_georef_lat, image_georef_lon] = ImageGeoref(61.7594604492, [40.7088053427 -77.9536902587], 244.54527785);
% row_idx = 19; col_idx = 107;

% 4/11/22- Flight 5, Pool 4 [37 8]
% [image_georef_lat, image_georef_lon] = ImageGeoref(66.2619628906, [40.7087895049 -77.9536927969], 245.56223468);
% row_idx = 37; col_idx = 8;



%% Image Georeferencing Including ROLL/PITCH FLIGHTS 4/22/22

% Flight 1, Pool 5
% [image_georef_lat, image_georef_lon, offset_UAS_center] = ImageGeoref_RollPitch(57.5000762939,[40.7087568068 -77.9536583602], 3.36667657729, 7.55428242202, 239.03237266);
% row_idx = 64; col_idx = 40;

% Flight 2, Pool 2
% [image_georef_lat, image_georef_lon, offset_UAS_center] = ImageGeoref_RollPitch(53.546081543,[40.7087515666 -77.9536450091], 1.50051357568, 3.93773645085, 241.322780332);
% row_idx = 68; col_idx = 143;

% Flight 3, Pool 3
% [image_georef_lat, image_georef_lon, offset_UAS_center] = ImageGeoref_RollPitch(49.0436553955,[40.708752618 -77.953642346], 1.95542155965, 3.53967670606, 238.197316458);
% row_idx = 53; col_idx = 83;

% Flight 4, Pool 4
% [image_georef_lat, image_georef_lon, offset_UAS_center] = ImageGeoref_RollPitch(49.5126800537,[40.7087435432 -77.9536416281], 2.46037987933, 5.42676731719, 242.083987021);
% row_idx = 25; col_idx = 26;

% Flight 5, Pool 1
% [image_georef_lat, image_georef_lon, offset_UAS_center] = ImageGeoref_RollPitch(47.6726379395,[40.7087592164 -77.9536267639], 2.56320255417, 1.5867012586, 247.52580449);
% row_idx = 15; col_idx = 156;

%% Image Georeferencing Including NO ROLL/PITCH FLIGHTS 4/22/22

% Flight 1, Pool 5
[image_georef_lat, image_georef_lon, pix_width, pix_height] = ImageGeoref(57.5000762939,[40.7087568068 -77.9536583602], 239.03237266);
row_idx = 64; col_idx = 40;

% Flight 2, Pool 2
% [image_georef_lat, image_georef_lon, pix_width, pix_height] = ImageGeoref(53.546081543,[40.7087515666 -77.9536450091], 241.322780332);
% row_idx = 68; col_idx = 143;

% Flight 3, Pool 3
% [image_georef_lat, image_georef_lon, pix_width, pix_height] = ImageGeoref(49.0436553955,[40.708752618 -77.953642346], 238.197316458);
% row_idx = 53; col_idx = 83;

% Flight 4, Pool 4
% [image_georef_lat, image_georef_lon, pix_width, pix_height] = ImageGeoref(49.5126800537,[40.7087435432 -77.9536416281], 242.083987021);
% row_idx = 25; col_idx = 26;

% Flight 5, Pool 1
% [image_georef_lat, image_georef_lon, pix_width, pix_height] = ImageGeoref(47.6726379395,[40.7087592164 -77.9536267639], 247.52580449);
% row_idx = 15; col_idx = 156;

%% Pool Location Testing
load('priority_waypoints.mat');

pool_num = input('Input pool location number\n');

%min_temp = min(min(tempvalues));
%[row_idx, col_idx] = find(min_temp == tempvalues);

% For manual override of pool location using ImageJ (row and col +1)
% row_idx = 64;
% col_idx = 41;

% calc_pool_lat = image_georef_lat(row_idx,col_idx);
% calc_pool_lon = image_georef_lon(row_idx,col_idx);

% ADDED ROUND to 9 digits from above^
calc_pool_lat = round(image_georef_lat(row_idx,col_idx),9);
calc_pool_lon = round(image_georef_lon(row_idx,col_idx),9);

known_pool_lat = priority_waypoints(pool_num,2);
known_pool_lon = priority_waypoints(pool_num,3);

pool_dist = distance(calc_pool_lat,calc_pool_lon,known_pool_lat,known_pool_lon,wgs84Ellipsoid);

lat_error = deg2km(abs(calc_pool_lat - known_pool_lat),'earth')*1000;
lon_error = deg2km(abs(calc_pool_lon - known_pool_lon),'earth')*1000;

geoscatter(calc_pool_lat,calc_pool_lon,'green'); % Calculated pool coords
hold on
geoscatter(known_pool_lat,known_pool_lon,'red'); % Known pool coords
hold on
%geoscatter(offset_UAS_center(1,1),offset_UAS_center(1,2),'x','g'); % Middle of image
hold on
%geoscatter(40.7087515666, -77.9536450091, 'x','r'); % UAS GPS position
geobasemap satellite

%% Plotter for GPS Coord Overlay
lat_lim = [40.70837 40.7091];
lon_lim = [-77.9541 -77.9533];
gx = geoaxes;

lat_list = reshape(image_georef_lat,[],1);
lon_list = reshape(image_georef_lon,[],1);
geoscatter(lat_list, lon_list,'.','r');
hold on
%geoscatter(lat_list(1,1),lon_list(1,1),'x','g')

geolimits(lat_lim,lon_lim);
title('No Roll/Pitch Corrections')
geobasemap satellite;
gx.Scalebar.Visible = 'off';
gx.LatitudeAxis.Visible = 'off';
gx.LongitudeAxis.Visible = 'off';

%% Georaster test
tiff = double(imread('Flight3.tiff'));
tiff = tiff * 0.01 - 273.15;

latlim = [min(min(image_georef_lat)) max(max(image_georef_lat))];
lonlim = [min(min(image_georef_lon)) max(max(image_georef_lon))];
raster_size = [120 160];

georefraster = georefcells(latlim,lonlim,raster_size);

geotiffwrite('test_tiff',tiff,georefraster)

geoshow('test_tiff.tif','DisplayType','texturemap')

%% Image Georeferencing- FUNCTION CODE

% Imported variables from UAS (or assumed for testing)
UAS_altitude = 75; % AGL altitude in meters
UAS_GPS_position = [40.708793 -77.953632]; % GPS position of drone when image is taken
UAS_heading = 331.292; % [Deg] Positive CW from North

% Variables for calculation (from FLIR Duo R specsheet)
cam_width_angle = 57; % Degrees
cam_depth_angle = 44; % Degrees
cam_width_pixels = 160; % Pixels
cam_depth_pixels = 120; % Pixels

% Georeferenced matrices equal to dimensions of FLIR Duo R pixel
% resolution. Two arrays, for latitude and longitude
image_georef_lat = zeros(cam_depth_pixels,cam_width_pixels);
image_georef_lon = zeros(cam_depth_pixels,cam_width_pixels);

% Calculate offset distance for corners
x_offset = tand(cam_width_angle/2) * UAS_altitude; % Horizontal offset in meters
y_offset = tand(cam_depth_angle/2) * UAS_altitude; % Vertical offset in meters
distance_offset = sqrt(x_offset^2 + y_offset^2); % Diagononal (Hypotenuse) distance in meters
distance_offset_deg =  rad2deg(distance_offset/earthRadius('m')); % Offset distance in Earth degrees

% Calculate azimuth angle for each corner offset (CW where #1 is top right)
az_1 = UAS_heading - (270 + tand(y_offset/x_offset) + 45);
az_2 = UAS_heading - (180 + tand(y_offset/x_offset) + 45);
az_3 = UAS_heading - (90 + tand(y_offset/x_offset) + 45);
az_4 = UAS_heading - (tand(y_offset/x_offset) + 45);

% Find corner coordinates using Reckon and write to Image GPS Matrices
image_georef_lat(cam_depth_pixels/2,cam_width_pixels/2) = UAS_GPS_position(1,1);
image_georef_lon(cam_depth_pixels/2,cam_width_pixels/2) = UAS_GPS_position(1,2);
[image_georef_lat(1,cam_width_pixels) image_georef_lon(1,cam_width_pixels)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2), distance_offset_deg, az_1);
[image_georef_lat(cam_depth_pixels,cam_width_pixels) image_georef_lon(cam_depth_pixels,cam_width_pixels)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2), distance_offset_deg, az_2);
[image_georef_lat(cam_depth_pixels,1) image_georef_lon(cam_depth_pixels,1)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2), distance_offset_deg, az_3);
[image_georef_lat(1,1) image_georef_lon(1,1)] = reckon(UAS_GPS_position(1,1),UAS_GPS_position(1,2), distance_offset_deg, az_4);

% Interpolate all pixel GPS coordinates between corners
image_georef_lat(1,:) = linspace(image_georef_lat(1,1), image_georef_lat(1,cam_width_pixels),cam_width_pixels);
image_georef_lat(cam_depth_pixels,:) = linspace(image_georef_lat(cam_depth_pixels,1), image_georef_lat(cam_depth_pixels,cam_width_pixels),cam_width_pixels);
for g = 1:cam_width_pixels;
    image_georef_lat(:,g) = linspace(image_georef_lat(1,g), image_georef_lat(cam_depth_pixels,g),cam_depth_pixels);
end
image_georef_lon(1,:) = linspace(image_georef_lon(1,1), image_georef_lon(1,cam_width_pixels),cam_width_pixels);
image_georef_lon(cam_depth_pixels,:) = linspace(image_georef_lon(cam_depth_pixels,1), image_georef_lon(cam_depth_pixels,cam_width_pixels),cam_width_pixels);
for g = 1:cam_width_pixels;
    image_georef_lon(:,g) = linspace(image_georef_lon(1,g), image_georef_lon(cam_depth_pixels,g),cam_depth_pixels);
end

%% Create Deficit Map- 3 dimensional image data array with Temp, Lat, Lon (respectively)

t_crit = -1.1; % Deg C for critical bud temp (CREATE LOOKUP TABLE FOR STAGE --> CRIT TEMP)
deficit_values = tempvalues - t_crit;
% Create 3 dimension matrix with deficit, latitude, longitude
Deficit_map = cat(3, double(deficit_values), image_georef_lat, image_georef_lon);

Lowest_temp = [];
Lowest_temp(1,1) = min(Deficit_map(:,:,1));
%[row col] = find
Lowest_temp(1,[2 3]) = find(min(min(Deficit_map(:,:,1))))


