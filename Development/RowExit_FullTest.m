function [exit_waypoint] = RowExit_FullTest(waypoints,next_waypoint,curr_waypoint,UGV_heading)
%

% Load row centerline coordinates
load('row_centers_lat.mat');
load('row_centers_lon.mat');
load('row_centerinterp_lat.mat');
load('row_centerinterp_lon.mat');

exit_waypoint = [];

% Heading parameters for row exit, if UGV heading ~= heading_SE, UGV is
% heading NW
heading_SE = [61 241]; % Deg CW from True North, was [16 285]

% Count number of waypoints
count = size(waypoints,1);

% Find current and next row location
dist_curr = distance(curr_waypoint(1,1),curr_waypoint(1,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
[~,col_curr] = find(dist_curr == min(min(dist_curr)));
dist_next = distance(next_waypoint(1,1),next_waypoint(1,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
[~,col_next] = find(dist_next == min(min(dist_next)));

% Test if the last waypoint mission had more than one point

% Find UGV's heading at end of the waypoint mission

if col_curr == col_next
    disp('Next waypoint is within the current row ahead of the UGV')
    exit_waypoint(1) = curr_waypoint(1);
    exit_waypoint(2) = curr_waypoint(2);

elseif (UGV_heading > heading_SE(1)) && (UGV_heading < heading_SE(2))
    disp('UGV is heading SE')
    
    exit_waypoint(1) = row_centers_lat(2,col_curr);
    exit_waypoint(2) = row_centers_lon(2,col_curr);

else
    disp('UGV is heading NW')

    exit_waypoint(1) = row_centers_lat(1,col_curr);
    exit_waypoint(2) = row_centers_lon(1,col_curr);

end


end