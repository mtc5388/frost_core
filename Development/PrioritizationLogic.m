%{ 
    Test Script for Prioritizing Waypoints
    Priority = higher deficit
%}

%% Load test waypoints
%priority_waypoints = table2array(readtable('Priority_Waypoints.xlsx','Range','A3:D7'));

% Number(1), lat(2), lon(3), path distance(4), deficit(5)
load('priority_waypoints.mat');

%% Create priority by sorting by deficit

wp_priority = sortrows(priority_waypoints,4,'descend')

geoscatter(priority_waypoints(:,2),priority_waypoints(:,3));
geobasemap satellite

%% Generate path plans for each waypoint- Single Points
UGV_start_lat = 40.709011256; 
UGV_start_lon = -77.953985410;
alt_start = 338.3908;

waypoint_paths = struct;
for k = 1:size(wp_priority,1)
     waypoint_paths(k).gps_coords = WaypointGenerator(UGV_start_lat,UGV_start_lon,alt_start,wp_priority(k,2),wp_priority(k,3));

     % Remove second to last waypoint to smooth path to final goal
     idx = size(waypoint_paths(k).gps_coords,1);
     waypoint_paths(k).gps_coords(idx-1,:) = [];

     geoplot(waypoint_paths(k).gps_coords(:,1),waypoint_paths(k).gps_coords(:,2),'-x');
     hold on
end
geobasemap satellite

%% Generate path plans for each waypoint- Multiple Point Nav
UGV_start_lat = 40.709011256; 
UGV_start_lon = -77.953985410;
alt_start = 338.3908;

waypoint_paths = struct;
for k = 1:size(wp_priority,1)
    %{
    After the first mission executes, find UGV heading and closest row
    end as exit using RowExit. Make row end the start of the next mission.
    %} 
    if k > 1
         end_wp = RowExit(waypoint_paths(k-1).gps_coords);
         UGV_start_lat = end_wp(1);
         UGV_start_lon = end_wp(2);
     end
    
     % Write waypoint paths to individual structures. Each path is numbered
     waypoint_paths(k).gps_coords = WaypointGenerator(UGV_start_lat,UGV_start_lon,alt_start,wp_priority(k,2),wp_priority(k,3));

     % Remove second to last waypoint to smooth path to final goal
     idx = size(waypoint_paths(k).gps_coords,1);
     waypoint_paths(k).gps_coords(idx-1,:) = [];

     geoplot(waypoint_paths(k).gps_coords(:,1),waypoint_paths(k).gps_coords(:,2),'-x');
     hold on

end
geobasemap satellite

%% Multi-Point 20 waypoint path test

tic
UGV_start_lat = 40.709011256; 
UGV_start_lon = -77.953985410;
alt_start = 338.3908;

priority_waypoints_multi = table2array(readtable('Priority_WaypointsTest.xlsx','Range','A3:D22'));
wp_priority_multi = sortrows(priority_waypoints_multi,4,'descend');


%waypoint_paths_multi = WaypointGenerator_MultiPointNav(UGV_start_lat,UGV_start_lon,alt_start,wp_priority_multi);
toc

%% Multi-Point 50 waypoint path test
clc, clear
num_wps = input('Enter how many waypoints to process\n');

tic
UGV_start_lat = 40.709011256;
UGV_start_lon = -77.953985410;
alt_start = 338.3908;

priority_waypoints_multi = table2array(readtable('Priority_WaypointsTest.xlsx','Range','A3:D52'));
wp_priority_multi = sortrows(priority_waypoints_multi,4,'descend');

wp_priority_multi_cropped = wp_priority_multi(1:num_wps,:);

waypoint_paths_multi = WaypointGenerator_MultiPointNav(UGV_start_lat,UGV_start_lon,alt_start,wp_priority_multi_cropped);
toc

%% Selective plot Multi-Point Nav
number = input("Input waypoint number\n");

lat_lim = [40.708367 40.709158];
lon_lim = [-77.954564 -77.952888];

plot = figure;
plot.WindowState = 'maximized';
geoplot(waypoint_paths_multi(number).gps_coords(:,1),waypoint_paths_multi(number).gps_coords(:,2),'--white');
hold on
% End waypoint of path in red, start in green
geoscatter(waypoint_paths_multi(number).gps_coords(end,1),waypoint_paths_multi(number).gps_coords(end,2),'red','filled');
hold on
geoscatter(waypoint_paths_multi(number).gps_coords(1,1),waypoint_paths_multi(number).gps_coords(1,2),'green','filled');
hold off
geolimits(lat_lim,lon_lim);
geobasemap satellite

%% Plot Multi-Point Nav
lat_lim = [40.70837 40.7091];
lon_lim = [-77.9541 -77.9533];
gx = geoaxes;

%size(wp_priority_multi,1)
for l = 19:20
    geoplot(waypoint_paths_multi(l).gps_coords(:,1),waypoint_paths_multi(l).gps_coords(:,2),'-.white','LineWidth',2);
    hold on
    geoscatter(waypoint_paths_multi(l).gps_coords(1,1),waypoint_paths_multi(l).gps_coords(1,2),70,'green','filled');
    hold on
    geoscatter(waypoint_paths_multi(l).gps_coords(end,1),waypoint_paths_multi(l).gps_coords(end,2),70,'red','filled');
    hold on
end
%title('Simulated Paths X-X','FontSize',16)
for b = 1:8
    geoplot(row_points_lat([1 160],b), row_points_lon([1 160],b), '-black','LineWidth',2)
end
geobasemap streets-dark
geolimits(lat_lim,lon_lim);
gx.Scalebar.Visible = 'off';
gx.LatitudeAxis.Visible = 'off';
gx.LongitudeAxis.Visible = 'off';
geotickformat -dd

%% Test Azimuth
num = 8;

heading = azimuth(waypoint_paths_multi(num).gps_coords(end-1,1),waypoint_paths_multi(num).gps_coords(end-1,2),waypoint_paths_multi(num).gps_coords(end,1),waypoint_paths_multi(num).gps_coords(end,2),wgs84Ellipsoid);
next_heading = azimuth(waypoint_paths_multi(num).gps_coords(end,1),waypoint_paths_multi(num).gps_coords(end,2),waypoint_paths_multi(num+1).gps_coords(1,1),waypoint_paths_multi(num+1).gps_coords(1,2),wgs84Ellipsoid);
diff = abs(next_heading - heading);

%% Generate 15 random GPS coords

rand_wpts = [];

for i = 1:15;
    idx = round(1 + (960-1).*rand(1,1),0);
    rand_wpts(i,1) = row_centerinterp_lat(idx);
    rand_wpts(i,2) = row_centerinterp_lon(idx);
end

geoscatter(rand_wpts(:,1),rand_wpts(:,2))
geobasemap satellite
