%{
Maxfield Canto, mtc5388
2/4/21
Description: MATLAB script to import and interpret the GPS data for Block
A1. Block A1 consists of 8 rows of apple trees starting (including) row EE. This script performs a 
transform of plot row locations from geographic coordinates to regional 
coordinates.
%}

%% Load GPS Data from Excel Document

%load('row_points_lat.mat');
%load('row_points_lon.mat');
rp_lat = table2array(readtable('row_points_lat.xlsx', 'Range', 'A1:H2'));
rp_lon = table2array(readtable('row_points_lon.xlsx', 'Range', 'A1:H2'));

%% Extend rows to account for wires
row_points_lat = [];
row_points_lon = [];
row_points_lat(1,:) = rp_lat(1,:);
row_points_lat(160,:) = rp_lat(2,:);
row_points_lon(1,:) = rp_lon(1,:);
row_points_lon(160,:) = rp_lon(2,:);


offset = 2; % meters (was 4)
offset_South = 0.1; % meters (was 2)
distUnits = 'm';
% Convert input distance to earth degrees (Lat, Lon are typicaly given in degrees)
arclen = rad2deg(offset/earthRadius(distUnits));
arclen_South = rad2deg(offset_South/earthRadius(distUnits));
az = 331.2919;
azrot = 331.2919 - 180;
for j = 1:size(row_points_lat,2)
    [row_points_lat(1,j), row_points_lon(1,j)] = reckon(row_points_lat(1,j), row_points_lon(1,j),arclen,az);
    [row_points_lat(160,j), row_points_lon(160,j)] = reckon(row_points_lat(160,j), row_points_lon(160,j),arclen_South,azrot);
    row_points_lat(2,j) = 0;
    row_points_lon(2,j) = 0;
end


%% Create Lat and Lon Row Point Matrices and Interpolate

InterpNumber = 160;

% Interp Latitude Matrix
for a = 1:size(row_points_lat,2)
    row_points_lat(:,a) = linspace(row_points_lat(1,a), row_points_lat(InterpNumber,a), InterpNumber);
end

% Interp Longitude Matrix
for a = 1:size(row_points_lon,2)
    row_points_lon(:,a) = linspace(row_points_lon(1,a), row_points_lon(InterpNumber,a), InterpNumber);
end

%%
row_centers_lat = table2array(readtable('row_centers_lat.xlsx', 'Range', 'A1:G2'));
row_centers_lon = table2array(readtable('row_centers_lon.xlsx', 'Range', 'A1:G2'));

%% Row centers

load('row_centers_lat.mat');
load('row_centers_lon.mat');

row_centerinterp_lat = []; row_centerinterp_lat(1,:) = row_centers_lat(1,:); row_centerinterp_lat(160,:) = row_centers_lat(2,:);
row_centerinterp_lon = []; row_centerinterp_lon(1,:) = row_centers_lon(1,:); row_centerinterp_lon(160,:) = row_centers_lon(2,:);

for a = 1:7
    row_centerinterp_lat(:,a) = linspace(row_centerinterp_lat(1,a), row_centerinterp_lat(160,a),160);
    row_centerinterp_lon(:,a) = linspace(row_centerinterp_lon(1,a), row_centerinterp_lon(160,a),160);
end

%% Plot

geoscatter(reshape(row_points_lat, [1280 1]) , reshape(row_points_lon, [1280 1]))
%geoscatter(reshape(row_centerinterp_lat, [160*7 1]), reshape(row_centerinterp_lon,[160*7 1]))
hold on
%geoscatter(reshape(rp_lat, [16 1]), reshape(rp_lon, [16 1]))
geobasemap satellite


%% Geodetic to Cartesian Transform

wgs84 = wgs84Ellipsoid;
alt = 338.3908; %meters HAE
origin_GPS = [40.708357, -77.953644 alt];

% Boundary starts at SW corner and runs clockwise
boundary = [40.708357, -77.953644;
            40.709039, -77.954126;
            40.709224, -77.953661;
            40.708565, -77.953153;];
        
[xBoundaryLocal, yBoundaryLocal] = geodetic2enu(boundary(:,1), boundary(:,2), alt, origin_GPS(1), origin_GPS(2), alt, wgs84);
[Xrows_local, Yrows_local] = geodetic2enu(row_points_lat, row_points_lon, alt, origin_GPS(1,1), origin_GPS(1,2), alt, wgs84);

%[Xrowscenter_local, Yrowscenter_local] = geodetic2enu(row_centerinterp_lat, row_centerinterp_lon, alt, origin_GPS(1,1), origin_GPS(1,2), alt, wgs84);

plot(Xrows_local, Yrows_local)
hold on
plot(xBoundaryLocal, yBoundaryLocal)
hold off

%% Rotate Cartesian Coordinates to 360deg True North Heading

heading = 331; % deg from True North (clockwise)

% Create rotation matrix
thetaR = heading - 360; % Offset angle of point headings to true North (360 deg)
Rot = [cosd(thetaR) -sind(thetaR); sind(thetaR) cosd(thetaR)];

% Rotate Boundary Coordinates
BoundaryPoints_Local = [xBoundaryLocal yBoundaryLocal]';
BoundaryPoints_RotLocal = Rot*BoundaryPoints_Local;

% Rotate Row Coordinates
RowPoints_RotLocal(1,:) = reshape(Xrows_local,[1 160*8]);
RowPoints_RotLocal(2,:) = reshape(Yrows_local,[1 160*8]);
RowPoints_RotLocal = Rot*RowPoints_RotLocal;

% Rotate Row Center Coords
% RowCenters_RotLocal(1,:) = reshape(Xrowscenter_local,[1 160*7]);
% RowCenters_RotLocal(2,:) = reshape(Yrowscenter_local,[1 160*7]);
% RowCenters_RotLocal = Rot*RowCenters_RotLocal;

plot(RowPoints_RotLocal(1,:),RowPoints_RotLocal(2,:), '.r')
hold on
plot(BoundaryPoints_RotLocal(1,:),BoundaryPoints_RotLocal(2,:))
hold off
% plot(RowCenters_RotLocal(1,:),RowCenters_RotLocal(2,:))
% hold off

%% Create Occupancy Grid Map for Navigation- TWO LAYERS

% Binary occupancy map for navigation
occupiedval = 1;
unoccupiedval = 0;
TreeRow_Map = binaryOccupancyMap(40,85,10);
setOccupancy(TreeRow_Map,BoundaryPoints_RotLocal', unoccupiedval);
%setOccupancy(TreeRow_Map,RowCenters_RotLocal',unoccupiedval);
setOccupancy(TreeRow_Map,RowPoints_RotLocal', occupiedval);
%INFLATION VALUE (radius in m)
inflate(TreeRow_Map, 1.1)
A1Block_OccupancyMap = figure;
show(TreeRow_Map)
hold off
% Occupancy map to matrix
TreeRow_MapMatrix = occupancyMatrix(TreeRow_Map);

%% Create second occupancy map with 


