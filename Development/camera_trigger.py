#Attach wire from Odroid pin 13 to Arduino pin A0

import RPi.GPIO as GPIO
import time
import sys
 
OutputPin = 27    # pin13, bcm27
 
def setup():
  GPIO.setmode(GPIO.BCM)       # Numbers GPIOs by chip numbering scheme
  GPIO.setup(OutputPin, GPIO.OUT)   # Set OutputPin's mode to output signal
 
def outputsignal():
  print("*****Outputting 3.3V Signal to Arduino*****\n")
  GPIO.output(OutputPin, GPIO.HIGH)  # output 3.3V to Arduino
  time.sleep(5)
    
 
def destroy():
  #GPIO.output(OutputPin, GPIO.LOW)   # output pin off
  #GPIO.setup(OutputPin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)   # Set LedPin's mode is input
  print("Cleaning Up the GPIO Resource")
  GPIO.cleanup()                  # Release resource
 
if __name__ == '__main__':     # Program start from here
  print('Connect wire from Odroid pin 13 to Arduino pin A0')
  print('Press Ctrl-C to exit') 
  setup()
  outputsignal()
  destroy()
  sys.exit('Exiting program')