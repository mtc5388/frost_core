%{
Maxfield Canto, mtc5388
2/4/21
Description: MATLAB script to import and interpret the GPS data for Block
A1. Block A1 consists of 19 rows of apple trees. This script performs a 
transform of plot row locations from geographic coordinates to regional 
coordinates.
%}

%% Load GPS Data from Excel Document

T = table2array(readtable('200305 rows.xlsx', 'Range', 'C33:F51'));

T2 = table2array(readtable('200305 rows.xlsx', 'Range', 'C36:F43'));

%% Row Center Line

center_line = zeros(18,4);
row_ends = zeros(18,4);
% constants
d2r = pi / 180.0;
% 1 deg lat = 364813 feet, 1 deg lon = cos(lat)*d2f_lat, MATLAB spherical Earth model
d2f_lat = 364813.0;  % units [ft/deg lat]
% overshoot at end of rows to handle bracing posts
offset = 15;  % units [ft]

a = 0;
for i = 1:height(T)-1
    a = a + 1;

    % Centerline between rows
    % South end waypoint
    latS = ( T(i,3) + T(i+1,3) )/ 2;
    lonS = ( T(i,4) + T(i+1,4) )/ 2;
    % North end waypoint
    latN = ( T(i,1) + T(i+1,1) )/ 2;
    lonN = ( T(i,2) + T(i+1,2) )/ 2;
    
    % path - x East, y North (delta lat and lon, find path length)
    del_lat = latN - latS;
    del_lon = lonN - lonS;

    d2f_lon = cos( latS*d2r ) * d2f_lat;  % units [ft/deg lon]
    del_x = del_lon * d2f_lon;            % units [ft]
    del_y = del_lat * d2f_lat;
    
    distance = sqrt( del_x*del_x + del_y*del_y );  % units [ft]       
    theta = atan2( del_y, del_x );                 % units [rad] - positive CCW from East - -pi<theta<=pi
    theta_deg = theta / d2r;                       % units [deg]
    heading = mod( (90 - theta_deg + 360), 360 );  % units [deg} - postivie CW from North
    
    % path in feet relative to first waypoint
    d1 = -offset;             % units [ft]
    dx1 = d1 * cos( theta );
    dy1 = d1 * sin( theta );

    d2 = offset;   % units [ft]
    dx2 = d2 * cos( theta );
    dy2 = d2 * sin( theta );

    % waypoints with overshoot - convert ft to deg relative to first waypoint
    center_line(i,3) = latS + dy1/d2f_lat;  % units [deg lat]
    center_line(i,4) = lonS + dx1/d2f_lon;  % units [deg lon]

    center_line(i,1) = latN + dy2/d2f_lat;
    center_line(i,2) = lonN + dx2/d2f_lon;
    
    % build row ends GPScoordinate matrix
    row_ends(i,1) = T(i,1);
    row_ends(i,2) = T(i,2);
    row_ends(i,3) = T(i,3);
    row_ends(i,4) = T(i,4);

end

%% Interpolation

InterpNumber = 160; % Number of points to interpolate on paths

% Interpolate X points per center line path
center_points = []
for r = 1:height(center_line);
    Cpoints = gcwaypts(center_line(r,1),center_line(r,2),center_line(r,3),center_line(r,4), InterpNumber);
    center_points = vertcat(center_points, Cpoints);
    
end

% Interpolate X points per row line
row_points = []
for r = 1:height(row_ends);
    Rpoints = gcwaypts(row_ends(r,1),row_ends(r,2),row_ends(r,3),row_ends(r,4), InterpNumber);
    row_points = vertcat(row_points, Rpoints);
    
end

%% Create GeoMap of Interp Row Points

A1Block_GeoMap = figure;
geoscatter(row_points(:,1), row_points(:,2), '.', 'r')
geobasemap satellite
hold off

%% Create Plot Using Local Euclidean Coordinate System

wgs84 = wgs84Ellipsoid;

% Boundary GPS Points- listed in clockwise descending order, starting at
% origin corner
boundary_GPS = [40.708322 -77.953748;
                40.708935, -77.954195;
                40.709252, -77.953446;
                40.708638, -77.953011]

alt = 1108; %[ft]
origin_Corner = [40.708322 -77.953748 alt] % Origin of approx corner on Southern end of Row B

[xBoundaryLocal, yBoundaryLocal] = geodetic2enu(boundary_GPS(:,1), boundary_GPS(:,2), alt, origin_Corner(1), origin_Corner(2), origin_Corner(3), wgs84);
[xRowEndsLocal,yRowEndsLocal,zUp] = geodetic2enu(row_ends(1:18,[1 3]), row_ends(1:18,[2 4]), alt, origin_Corner(1), origin_Corner(2), origin_Corner(3), wgs84);
[xRowPointsLocal, yRowPointsLocal] = geodetic2enu(row_points(:,1), row_points(:,2), alt, origin_Corner(1), origin_Corner(2), origin_Corner(3), wgs84);
[xCenterEndsLocal,yCenterEndsLocal,zUp] = geodetic2enu(center_line(1:18,[1 3]), center_line(1:18,[2 4]), alt, origin_Corner(1), origin_Corner(2), origin_Corner(3), wgs84);
[xCenterPointsLocal,yCenterPointsLocal,zUp] = geodetic2enu(center_points(:,1), center_points(:,2), alt, origin_Corner(1), origin_Corner(2), origin_Corner(3), wgs84);

% Creates points between each row to draw the center path lines when
% plotting
xCenterPathLocal = [xCenterEndsLocal(:,1) xCenterEndsLocal(:,2)];
yCenterPathLocal = [yCenterEndsLocal(:,1) yCenterEndsLocal(:,2)];

% PLOTTING
A1Block_Cartesian = figure;
% Boundary coordinates
plot(xBoundaryLocal, yBoundaryLocal)
hold on;
% Non-offset tree row ends
plot(xRowEndsLocal,yRowEndsLocal, 'x')
hold on;
% Offset center path ends
plot(xCenterEndsLocal,yCenterEndsLocal, 'o')
hold on;
% Center path lines- JUST FOR VISUAL
plot(xCenterPathLocal',yCenterPathLocal');
hold on;
% Interpolated row line points
plot(xRowPointsLocal,yRowPointsLocal, 'x')
hold on
% Interpolated center line points
plot(xCenterPointsLocal, yCenterPointsLocal, 'o');
hold off
axis('equal')

%% Rotate Cartesian Coordinates to 360deg True North Heading

% Create rotation matrix
thetaR = heading - 360; % Offset angle of point headings to true North (360 deg)
Rot = [cosd(thetaR) -sind(thetaR); sind(thetaR) cosd(thetaR)];
% Rotate Boundary Coordinates
BoundaryPoints_Local = [xBoundaryLocal yBoundaryLocal]';
BoundaryPoints_RotLocal = Rot*BoundaryPoints_Local;
% Rotate Row Point Coordinates
RowPoints_Local = [xRowPointsLocal yRowPointsLocal]';
RowPoints_RotLocal = Rot*RowPoints_Local;

% ROTATED PLOT
A1Block_RotatedCartesian = figure;
plot(RowPoints_RotLocal(1,:), RowPoints_RotLocal(2,:), '.r');
title('Tree Rows in Rotated Cartesian Coordinate Frame');
xlabel('X [meters]');
ylabel('Y [meters]');
hold on
%plot(BoundaryPoints_RotLocal(1,:), BoundaryPoints_RotLocal(2,:));
hold off

%% Create Occupancy Grid Map for Navigation- TWO LAYERS

% Binary occupancy map for navigation
occupiedval = 1;
unoccupiedval = 0;
TreeRow_Map = binaryOccupancyMap(75,75,10);
setOccupancy(TreeRow_Map,BoundaryPoints_RotLocal', unoccupiedval);
setOccupancy(TreeRow_Map,RowPoints_RotLocal', occupiedval);
%INFLATION VALUE (radius in m?)
inflate(TreeRow_Map, 0.75)
A1Block_OccupancyMap = figure;
show(TreeRow_Map)
hold off
% Occupancy map to matrix
TreeRow_MapMatrix = occupancyMatrix(TreeRow_Map);

%% Build initial Deficit map using binary tree row locations
% INPUT assumed bud stage (convert to crit temp, lookup) and ambient air temperature. MAKE LOOKUP for bud
% stages
budstage_crittemp = -1.1; % Deg C, to be changed
ambientair_temp = 1.5; % Deg C
initial_deficitmatrix = abs(budstage_crittemp - ambientair_temp) * TreeRow_MapMatrix;

InitialDeficit = figure;
surf(initial_deficitmatrix);
colormap('autumn');
hold off


%% Determine Closest Center Row Coordinate to Fake Temp Value
% Upload fake GPS location of a critical temperature region
fake_lat = 40.708781;
fake_lon = -77.953667;

% Find closest lat/lon within UGV center line path
[min_dist_lat, lat_idx]  = min(abs(center_points(:,1) - fake_lat));
closest_lat = center_points(lat_idx,1);
[min_dist_lon, lon_idx]  = min(abs(center_points(:,2) - fake_lon));
closest_lon = center_points(lon_idx,2);


%% Cooling Rate Logic



%% Raster (To delete later)
%{
% Raster reference object
% Latitudes of Northern B Row and Southern O Row
latlim = [40.708362 40.7091793];
% Longitudes of Northern B Row and Southern O Row
lonlim = [-77.9541298 -77.9530693];
% Raster grid size (rows, columns)
rasterSize = [50 50]
% Create Raster Object
R = georefcells(latlim, lonlim, rasterSize)
[lat, lon] = geographicGrid(R)
%}

%{ 
Vehicle Cost map
occupiedval = 1;
TreeRow_Map = vehicleCostmap(85,75,0);
setCosts(TreeRow_Map,RowPoints_RotLocal', occupiedval)
plot(TreeRow_Map)
%}
