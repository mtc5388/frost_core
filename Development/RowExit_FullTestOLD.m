function [exit_waypoint] = RowExit_FullTestOLD(waypoints,next_waypoint,curr_waypoint)
%

% Load row centerline coordinates
load('row_centers_lat.mat');
load('row_centers_lon.mat');
load('row_centerinterp_lat.mat');
load('row_centerinterp_lon.mat');

exit_waypoint = [];

% Heading parameters for row exit, if UGV heading ~= heading_SE, UGV is
% heading NW
heading_SE = [61 241]; % Deg CW from True North, was [16 285]

% Count number of waypoints
count = size(waypoints,1);

% Find current and next row location
dist_curr = distance(curr_waypoint(1,1),curr_waypoint(1,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
[~,col_curr] = find(dist_curr == min(min(dist_curr)));
dist_next = distance(next_waypoint(1,1),next_waypoint(1,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
[~,col_next] = find(dist_next == min(min(dist_next)));

% Find UGV's heading at end of the waypoint mission
UGV_heading = azimuth(waypoints(count-1,1),waypoints(count-1,2),waypoints(count,1),waypoints(count,2),wgs84Ellipsoid);

if col_curr == col_next
    disp('Next waypoint is within the current row ahead of the UGV')
    exit_waypoint(1) = curr_waypoint(1);
    exit_waypoint(2) = curr_waypoint(2);

elseif (UGV_heading > heading_SE(1)) && (UGV_heading < heading_SE(2))
    disp('UGV is heading SE')
    
    exit_waypoint(1) = row_centers_lat(2,col_curr);
    exit_waypoint(2) = row_centers_lon(2,col_curr);

else
    disp('UGV is heading NW')

    exit_waypoint(1) = row_centers_lat(1,col_curr);
    exit_waypoint(2) = row_centers_lon(1,col_curr);

end


% Test heading to find direction, extract closest row center point to exit
% row end
if (UGV_heading > heading_SE(1)) && (UGV_heading < heading_SE(2))
    disp('UGV is heading SE')

    % Test if point is in row, infront of UGV
    %point_heading = azimuth(waypoints(end,1),waypoints(end,2),next_waypoints(1,1),next_waypoints(1,2),wgs84Ellipsoid);
    %heading_diff = abs(point_heading - UGV_heading);
    % Test what row the current and next point are in
    dist_curr = distance(curr_waypoint(1,1),curr_waypoint(1,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
    [~,col_curr] = find(dist_curr == min(min(dist_curr)));
    dist_next = distance(next_waypoint(1,1),next_waypoint(1,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
    [~,col_next] = find(dist_next == min(min(dist_next)));

    if col_curr == col_next
        
        disp('Next point is within the current row')
        % exit waypoint is current UGV position
        exit_waypoint(1) = waypoints(end,1);
        exit_waypoint(2) = waypoints(end,2);

    else

        dist = distance(waypoints(count,1),waypoints(count,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
        [~,col] = find(dist == min(min(dist)));
        
        exit_waypoint(1) = row_centers_lat(2,col);
        exit_waypoint(2) = row_centers_lon(2,col);
    
    end

else
    disp('UGV is heading NW')
    
    % Test if point is in row, infront of UGV
    %point_heading = azimuth(waypoints(end,1),waypoints(end,2),next_waypoints(1,1),next_waypoints(1,2),wgs84Ellipsoid);
    %heading_diff = abs(point_heading - UGV_heading);

    dist_curr = distance(curr_waypoint(1,1),curr_waypoint(1,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
    [~,col_curr] = find(dist_curr == min(min(dist_curr)));
    dist_next = distance(next_waypoint(1,1),next_waypoint(1,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
    [~,col_next] = find(dist_next == min(min(dist_next)));

    if col_curr == col_next
        
        disp('Next point is within the current row')
        
        % exit waypoint is current UGV position
        exit_waypoint(1) = waypoints(end,1);
        exit_waypoint(2) = waypoints(end,2);

    else

        dist = distance(waypoints(count,1),waypoints(count,2),row_centerinterp_lat,row_centerinterp_lon,wgs84Ellipsoid);
        [~,col] = find(dist == min(min(dist)));
    
        exit_waypoint(1) = row_centers_lat(1,col);
        exit_waypoint(2) = row_centers_lon(1,col);
    end
end    


end