%% Take waypoints from waypoint_paths structure and write to .waypoints file

pool_number = input("Enter the pool location number (1-5)\n");

[path_number,~] = find(wp_priority == pool_number);
wait_time = 0.15; % Parameter 1, wait time at each point
alt = 0; % Relative to launch

% Open document and print header
doc = fopen("UGV_A1_Multi_P" + pool_number + ".waypoints", 'wt');
fprintf(doc,'QGC WPL 110\n');

% Loop through and print all waypoints to document
a = 0;
wp_num = 1;
for i = 1:size(waypoint_paths(path_number).gps_coords)
    if a == 0;
        b = 1;
    else
        b = 0;
    end
    fprintf(doc,'%d \t %d \t 3 \t 16 \t %.2f \t 0 \t 0 \t NaN \t %.15f \t %.15f \t %.4f \t 1 \n', a, b, wait_time, waypoint_paths(path_number).gps_coords(wp_num,1),waypoint_paths(path_number).gps_coords(wp_num,2),alt);
    
    wp_num = wp_num + 1;
    a = a + 1;
end

% Close document
fclose(doc);