
%% Disconnect from MAVLink
disconnect(mavlink,"UDP")

%% Set up MAVLink Connection

mavlink = mavlinkio("common.xml");
UGV_Connection = connect(mavlink,"UDP");

%% Check for connection

MavClient_Name = listClients(mavlink)
MavConnection_Name = listConnections(mavlink)

%Create the object for storing the client information. Qground system ID is
% 255 and component ID is 1.
client = mavlinkclient(mavlink,255,1);

%% Heartbeat message

% Create subscriber
heartbeat = mavlinksub(mavlink,client,'HEARTBEAT');

% Get latest Hearbeat message
latest_heartbeat = latestmsgs(heartbeat,1)