function [goal_GPS, pathlength] = WaypointGenerator_PathTests(start_ENU_X, start_ENU_Y, goal_ENU_X, goal_ENU_Y, TreeRow_PRM_NodeMap)
%{
    Function follows these steps:
    1. Takes in current UGV GPS location and goal coordinates.
    2. Translates coords to ENU coord frame and rotates with matrix.
    3. Passes through PRM map to find path and creates waypoints.
    4. Translates the waypoints back to GPS coords 
    5. Outputs these as a waypoint list.
%}

% Load PRM Map
%load('TreeRow_PRM_NodeMap.mat');

% Origin point
alt = 337.72;
origin = [40.708357 -77.953644 alt];
Zup = -0.000171116569163132;

% Translate to ENU and Rotate
field_heading = 331.2919899401670;
thetaR = field_heading - 360; % Offset angle of point headings to true North (360 deg)
Rot = [cosd(thetaR) -sind(thetaR); sind(thetaR) cosd(thetaR)];
thetaL = 360 - field_heading;
RotRev = [cosd(thetaL) -sind(thetaL); sind(thetaL) cosd(thetaL)];

% Inject start and goal coords
start_ENU = [start_ENU_X start_ENU_Y];
goal_ENU = [goal_ENU_X goal_ENU_Y];

% Pass points into PRM map to make waypoints
WP = findpath(TreeRow_PRM_NodeMap,start_ENU, goal_ENU);

% Rotate and convert waypoints back to GPS coords
waypoints_Rot = RotRev * WP';
[waypoints_GPS(:,1), waypoints_GPS(:,2)] = enu2geodetic(waypoints_Rot(1,:), waypoints_Rot(2,:), Zup, origin(1), origin(2), alt, wgs84Ellipsoid);

waypoints = waypoints_GPS;
[count,~] = size(waypoints);

goal_GPS = waypoints(count,:);

pathlength = 0;
for i = 1:count-1
    len = distance(waypoints(i,1),waypoints(i,2),waypoints(i+1,1),waypoints(i+1,2),wgs84Ellipsoid);
    pathlength = pathlength + len;
    i = i + 1;
end

end