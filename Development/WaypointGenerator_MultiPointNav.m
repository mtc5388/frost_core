function [waypoint_paths] = WaypointGenerator_MultiPointNav(UGV_start_lat,UGV_start_lon,alt_start,waypoint_list)
% Generates a "waypoint_paths" structure for the UGV using inputs


waypoint_paths = struct;
for k = 1:size(waypoint_list,1)
    %{
    After the first mission executes, find UGV heading and closest row
    end as exit using RowExit. Make row end the start of the next mission.
    %}
    if k > 1
        sz_previous = size(waypoint_paths(k-1).gps_coords,1);
        if sz_previous <= 2
            UGV_heading = azimuth(waypoint_paths(k-2).gps_coords(end,1),waypoint_paths(k-2).gps_coords(end,2),waypoint_paths(k-1).gps_coords(end,1),waypoint_paths(k-1).gps_coords(end,2),wgs84Ellipsoid);
        else
            UGV_heading = azimuth(waypoint_paths(k-1).gps_coords(end-1,1),waypoint_paths(k-1).gps_coords(end-1,2),waypoint_paths(k-1).gps_coords(end,1),waypoint_paths(k-1).gps_coords(end,2),wgs84Ellipsoid);
        end
        if k < size(waypoint_list,1)
            % Changed from FullTest to FullTestOLD and removed the
            % UGV_heading input
            end_wp = RowExit_FullTestOLD(waypoint_paths(k-1).gps_coords,waypoint_list(k+1,[2 3]),waypoint_list(k,[2 3]));
            UGV_start_lat = end_wp(1);
            UGV_start_lon = end_wp(2);
        else
            end_wp = RowExit(waypoint_paths(k-1).gps_coords);
            UGV_start_lat = end_wp(1);
            UGV_start_lon = end_wp(2);
        end
    end
    
     % Write waypoint paths to individual structures. Each path is numbered
    waypoint_paths(k).gps_coords = WaypointGenerator(UGV_start_lat,UGV_start_lon,alt_start,waypoint_list(k,2),waypoint_list(k,3));

    % Remove second to last waypoint to smooth path to final goal
    idx = size(waypoint_paths(k).gps_coords,1);
    if idx > 3
        waypoint_paths(k).gps_coords(idx-1,:) = [];
    else
    end
end



end