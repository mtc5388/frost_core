%{
    ROS master will 
%}
%% Clear main ROS core
stopNode(UAS_Device,'dji_sdk_node');
rosshutdown
clear('core')

%% Set up environment variables
IP.Base = '172.16.0.3';
IP.UAS = '172.16.0.2';

master_URI = getenv('ROS_MASTER_URI');
hostname = getenv('ROS_HOSTNAME');
master_IP = getenv('ROS_IP');
setenv('ROS_MASTER_URI',"http://172.16.0.3:11311");
setenv('ROS_IP','172.16.0.3');


%% Launch ROS on base station computer
%~/catkin_ws/src/Onboard-SDK-ROS/dji_sdk/include/dji_sdk

core = ros.Core
rosinit("http://"+IP.Base+":11311",'NodeName','/base_node')
%base_node = ros.Node('/base_node',Base_IP,11311)

%% Set Up SDK Node Parameters

rosparam set '/dji_sdk/serial_name' '/dev/ttyUSB0'
rosparam set '/dji_sdk/baud_rate' '230400'
rosparam set '/dji_sdk/app_id' '1074714'
rosparam set '/dji_sdk/app_version' '1'
rosparam set '/dji_sdk/enc_key' 'eeeea813985b6b3a5620156986f4b89913b0dfd88178d04790232faf4cbd01702'
rosparam set '/dji_sdk/align_time' 'false'
rosparam set '/dji_sdk/use_broadcast' 'false'

%% Launch UAS ROS SDK

UAS_Device = rosdevice(IP.UAS,'odroid');
runNode(UAS_Device,'dji_sdk_node','http://172.16.0.3:11311');

rosnode list

%% Log UAS flight data

% Create subscriber nodes to pull GPS, attitude, and altitude
GPS_sub = rossubscriber('/dji_sdk/gps_position','sensor_msgs/NavSatFix','DataFormat','struct');
Attitude_sub = rossubscriber('/dji_sdk/attitude','geometry_msgs/QuaternionStamped','DataFormat','struct');
Altitude_sub = rossubscriber('/dji_sdk/height_above_takeoff','std_msgs/Float32','DataFormat','struct');

% Recieve and log data from sub nodes
[latlon,GPS_status,~] = receive(GPS_sub,10);
[att,attitude_status,~] = receive(Attitude_sub,10);
[alt,altitude_status,~] = receive(Altitude_sub,10);


%% Convert Recovered Data to Correct Units

euler = (180/3.141592) * quat2eul([att.Quaternion.X att.Quaternion.Y att.Quaternion.Z att.Quaternion.W]);
attitude = euler(1);