// Pin designations
const int CameraPin = 3;
int triggerState = 0;
// Delay values (in ms)
const int lowDelay = 1;
const int highDelay = 2;
const int offDelay = 20;
// Low and High variables
const int off = 0;
const int on = 255;

void setup() {
  pinMode(CameraPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    for(int j = 0; j < 300; j++) {
      digitalWrite(LED_BUILTIN, HIGH);
      for(int i = 0; i < 5; i++) {
        analogWrite(CameraPin, on);
        delay(highDelay);
        analogWrite(CameraPin, off);
        delay(offDelay);
      }
      delay(10);
      digitalWrite(LED_BUILTIN, LOW);
      for(int j = 0; j < 5; j++){
        analogWrite(CameraPin, on);
        delay(lowDelay);
        analogWrite(CameraPin, off);
        delay(offDelay);
      }
      delay(29000);
    }
}  
