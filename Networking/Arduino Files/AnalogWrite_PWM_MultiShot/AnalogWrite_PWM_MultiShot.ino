/* 
 *  Written by: Maxfield Canto, 2/4/22
 *  maxfcanto@gmail.com
 *  The Pennsylvania State University
 *  Script to recieve an input signal from Odroid computer onboard a DJI drone
 *  and trigger a single image capture of a FLIR Duo R. LED onboard the
 *  Arduino will blink while searching until the camera is triggered and 
 *  then will turn off and exit the program. Follow pin designations for
 *  plugging in devices.
 */

// Pin designations
const int CameraPin = 3;
const int OdroidPin = A0;
int triggerState = 0;
// Delay values (in ms)
const int lowDelay = 1;
const int highDelay = 2;
const int offDelay = 20;
// Low and High variables
const int off = 0;
const int on = 255;

void setup() {
  pinMode(CameraPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  triggerState = analogRead(OdroidPin);
  if (triggerState >= 340) {
    digitalWrite(LED_BUILTIN, LOW);
    for(int i = 0; i < 5; i++) {
      analogWrite(CameraPin, on);
      delay(highDelay);
      analogWrite(CameraPin, off);
      delay(offDelay);
    }
    delay(1000);  
  } 
  else {
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(100);
    for(int j = 0; j < 5; j++){
      analogWrite(CameraPin, on);
      delay(lowDelay);
      analogWrite(CameraPin, off);
      delay(offDelay);
    }
  }
}  
