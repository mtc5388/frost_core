/*  Written by: Maxfield Canto, 2/4/2022
 *  maxfcanto@gmail.com
 *  The Pennsylvania State University
 *  Description: Script will poll the analogIn pin A0 to check for values and write to the serial monitor.
 *  The standard reading with no input should be between 0-100. The camera will not trigger until the value
 *  is greater than 250. The Odroid pulse should be ~300 when the camera_trigger.py program is run.
 *  
*/

const int analogInPin = A0;
int inputValue = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  inputValue = analogRead(analogInPin);
  Serial.print("sensor = ");
  Serial.println(inputValue);
  delay(1000);
}
