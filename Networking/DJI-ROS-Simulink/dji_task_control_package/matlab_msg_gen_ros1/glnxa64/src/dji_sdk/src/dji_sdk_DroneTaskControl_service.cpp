// Copyright 2019-2020 The MathWorks, Inc.
// Common copy functions for dji_sdk/DroneTaskControlRequest
#include "boost/date_time.hpp"
#include "boost/shared_array.hpp"
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4244)
#pragma warning(disable : 4265)
#pragma warning(disable : 4458)
#pragma warning(disable : 4100)
#pragma warning(disable : 4127)
#pragma warning(disable : 4267)
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#pragma GCC diagnostic ignored "-Wdelete-non-virtual-dtor"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wshadow"
#endif //_MSC_VER
#include "ros/ros.h"
#include "dji_sdk/DroneTaskControl.h"
#include "visibility_control.h"
#include "class_loader/multi_library_class_loader.hpp"
#include "ROSPubSubTemplates.hpp"
#include "ROSServiceTemplates.hpp"
class DJI_SDK_EXPORT dji_sdk_msg_DroneTaskControlRequest_common : public MATLABROSMsgInterface<dji_sdk::DroneTaskControl::Request> {
  public:
    virtual ~dji_sdk_msg_DroneTaskControlRequest_common(){}
    virtual void copy_from_struct(dji_sdk::DroneTaskControl::Request* msg, const matlab::data::Struct& arr, MultiLibLoader loader); 
    //----------------------------------------------------------------------------
    virtual MDArray_T get_arr(MDFactory_T& factory, const dji_sdk::DroneTaskControl::Request* msg, MultiLibLoader loader, size_t size = 1);
};
  void dji_sdk_msg_DroneTaskControlRequest_common::copy_from_struct(dji_sdk::DroneTaskControl::Request* msg, const matlab::data::Struct& arr,
               MultiLibLoader loader) {
    try {
        //task
        const matlab::data::TypedArray<uint8_t> task_arr = arr["Task"];
        msg->task = task_arr[0];
    } catch (matlab::data::InvalidFieldNameException&) {
        throw std::invalid_argument("Field 'Task' is missing.");
    } catch (matlab::Exception&) {
        throw std::invalid_argument("Field 'Task' is wrong type; expected a uint8.");
    }
  }
  //----------------------------------------------------------------------------
  MDArray_T dji_sdk_msg_DroneTaskControlRequest_common::get_arr(MDFactory_T& factory, const dji_sdk::DroneTaskControl::Request* msg,
       MultiLibLoader loader, size_t size) {
    auto outArray = factory.createStructArray({size,1},{"MessageType","TASKGOHOME","TASKTAKEOFF","TASKLAND","Task"});
    for(size_t ctr = 0; ctr < size; ctr++){
    outArray[ctr]["MessageType"] = factory.createCharArray("dji_sdk/DroneTaskControlRequest");
    // TASK_GOHOME
    auto currentElement_TASK_GOHOME = (msg + ctr)->TASK_GOHOME;
    outArray[ctr]["TASKGOHOME"] = factory.createScalar(static_cast<uint8_t>(currentElement_TASK_GOHOME));
    // TASK_TAKEOFF
    auto currentElement_TASK_TAKEOFF = (msg + ctr)->TASK_TAKEOFF;
    outArray[ctr]["TASKTAKEOFF"] = factory.createScalar(static_cast<uint8_t>(currentElement_TASK_TAKEOFF));
    // TASK_LAND
    auto currentElement_TASK_LAND = (msg + ctr)->TASK_LAND;
    outArray[ctr]["TASKLAND"] = factory.createScalar(static_cast<uint8_t>(currentElement_TASK_LAND));
    // task
    auto currentElement_task = (msg + ctr)->task;
    outArray[ctr]["Task"] = factory.createScalar(currentElement_task);
    }
    return std::move(outArray);
  }
class DJI_SDK_EXPORT dji_sdk_msg_DroneTaskControlResponse_common : public MATLABROSMsgInterface<dji_sdk::DroneTaskControl::Response> {
  public:
    virtual ~dji_sdk_msg_DroneTaskControlResponse_common(){}
    virtual void copy_from_struct(dji_sdk::DroneTaskControl::Response* msg, const matlab::data::Struct& arr, MultiLibLoader loader); 
    //----------------------------------------------------------------------------
    virtual MDArray_T get_arr(MDFactory_T& factory, const dji_sdk::DroneTaskControl::Response* msg, MultiLibLoader loader, size_t size = 1);
};
  void dji_sdk_msg_DroneTaskControlResponse_common::copy_from_struct(dji_sdk::DroneTaskControl::Response* msg, const matlab::data::Struct& arr,
               MultiLibLoader loader) {
    try {
        //result
        const matlab::data::TypedArray<bool> result_arr = arr["Result"];
        msg->result = result_arr[0];
    } catch (matlab::data::InvalidFieldNameException&) {
        throw std::invalid_argument("Field 'Result' is missing.");
    } catch (matlab::Exception&) {
        throw std::invalid_argument("Field 'Result' is wrong type; expected a logical.");
    }
    try {
        //cmd_set
        const matlab::data::TypedArray<uint8_t> cmd_set_arr = arr["CmdSet"];
        msg->cmd_set = cmd_set_arr[0];
    } catch (matlab::data::InvalidFieldNameException&) {
        throw std::invalid_argument("Field 'CmdSet' is missing.");
    } catch (matlab::Exception&) {
        throw std::invalid_argument("Field 'CmdSet' is wrong type; expected a uint8.");
    }
    try {
        //cmd_id
        const matlab::data::TypedArray<uint8_t> cmd_id_arr = arr["CmdId"];
        msg->cmd_id = cmd_id_arr[0];
    } catch (matlab::data::InvalidFieldNameException&) {
        throw std::invalid_argument("Field 'CmdId' is missing.");
    } catch (matlab::Exception&) {
        throw std::invalid_argument("Field 'CmdId' is wrong type; expected a uint8.");
    }
    try {
        //ack_data
        const matlab::data::TypedArray<uint32_t> ack_data_arr = arr["AckData"];
        msg->ack_data = ack_data_arr[0];
    } catch (matlab::data::InvalidFieldNameException&) {
        throw std::invalid_argument("Field 'AckData' is missing.");
    } catch (matlab::Exception&) {
        throw std::invalid_argument("Field 'AckData' is wrong type; expected a uint32.");
    }
  }
  //----------------------------------------------------------------------------
  MDArray_T dji_sdk_msg_DroneTaskControlResponse_common::get_arr(MDFactory_T& factory, const dji_sdk::DroneTaskControl::Response* msg,
       MultiLibLoader loader, size_t size) {
    auto outArray = factory.createStructArray({size,1},{"MessageType","Result","CmdSet","CmdId","AckData"});
    for(size_t ctr = 0; ctr < size; ctr++){
    outArray[ctr]["MessageType"] = factory.createCharArray("dji_sdk/DroneTaskControlResponse");
    // result
    auto currentElement_result = (msg + ctr)->result;
    outArray[ctr]["Result"] = factory.createScalar(static_cast<bool>(currentElement_result));
    // cmd_set
    auto currentElement_cmd_set = (msg + ctr)->cmd_set;
    outArray[ctr]["CmdSet"] = factory.createScalar(currentElement_cmd_set);
    // cmd_id
    auto currentElement_cmd_id = (msg + ctr)->cmd_id;
    outArray[ctr]["CmdId"] = factory.createScalar(currentElement_cmd_id);
    // ack_data
    auto currentElement_ack_data = (msg + ctr)->ack_data;
    outArray[ctr]["AckData"] = factory.createScalar(currentElement_ack_data);
    }
    return std::move(outArray);
  } 
class DJI_SDK_EXPORT dji_sdk_DroneTaskControl_service : public ROSMsgElementInterfaceFactory {
  public:
    virtual ~dji_sdk_DroneTaskControl_service(){}
    virtual std::shared_ptr<MATLABPublisherInterface> generatePublisherInterface(ElementType type);
    virtual std::shared_ptr<MATLABSubscriberInterface> generateSubscriberInterface(ElementType type);
    virtual std::shared_ptr<MATLABSvcServerInterface> generateSvcServerInterface();
    virtual std::shared_ptr<MATLABSvcClientInterface> generateSvcClientInterface();
};  
  std::shared_ptr<MATLABPublisherInterface> 
          dji_sdk_DroneTaskControl_service::generatePublisherInterface(ElementType type){
    std::shared_ptr<MATLABPublisherInterface> ptr;
    if(type == eRequest){
        ptr = std::make_shared<ROSPublisherImpl<dji_sdk::DroneTaskControl::Request,dji_sdk_msg_DroneTaskControlRequest_common>>();
    }else if(type == eResponse){
        ptr = std::make_shared<ROSPublisherImpl<dji_sdk::DroneTaskControl::Response,dji_sdk_msg_DroneTaskControlResponse_common>>();
    }else{
        throw std::invalid_argument("Wrong input, Expected 'Request' or 'Response'");
    }
    return ptr;
  }
  std::shared_ptr<MATLABSubscriberInterface> 
          dji_sdk_DroneTaskControl_service::generateSubscriberInterface(ElementType type){
    std::shared_ptr<MATLABSubscriberInterface> ptr;
    if(type == eRequest){
        ptr = std::make_shared<ROSSubscriberImpl<dji_sdk::DroneTaskControl::Request,dji_sdk::DroneTaskControl::Request::ConstPtr,dji_sdk_msg_DroneTaskControlRequest_common>>();
    }else if(type == eResponse){
        ptr = std::make_shared<ROSSubscriberImpl<dji_sdk::DroneTaskControl::Response,dji_sdk::DroneTaskControl::Response::ConstPtr,dji_sdk_msg_DroneTaskControlResponse_common>>();
    }else{
        throw std::invalid_argument("Wrong input, Expected 'Request' or 'Response'");
    }
    return ptr;
  }
  std::shared_ptr<MATLABSvcServerInterface> 
          dji_sdk_DroneTaskControl_service::generateSvcServerInterface(){
    return std::make_shared<ROSSvcServerImpl<dji_sdk::DroneTaskControl::Request,dji_sdk::DroneTaskControl::Response,dji_sdk_msg_DroneTaskControlRequest_common,dji_sdk_msg_DroneTaskControlResponse_common>>();
  }
  std::shared_ptr<MATLABSvcClientInterface> 
          dji_sdk_DroneTaskControl_service::generateSvcClientInterface(){
    return std::make_shared<ROSSvcClientImpl<dji_sdk::DroneTaskControl,dji_sdk::DroneTaskControl::Request,dji_sdk::DroneTaskControl::Response,dji_sdk_msg_DroneTaskControlRequest_common,dji_sdk_msg_DroneTaskControlResponse_common>>();
  }
#include "class_loader/register_macro.hpp"
// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
CLASS_LOADER_REGISTER_CLASS(dji_sdk_msg_DroneTaskControlRequest_common, MATLABROSMsgInterface<dji_sdk::DroneTaskControl::Request>)
CLASS_LOADER_REGISTER_CLASS(dji_sdk_msg_DroneTaskControlResponse_common, MATLABROSMsgInterface<dji_sdk::DroneTaskControl::Response>)
CLASS_LOADER_REGISTER_CLASS(dji_sdk_DroneTaskControl_service, ROSMsgElementInterfaceFactory)
#ifdef _MSC_VER
#pragma warning(pop)
#else
#pragma GCC diagnostic pop
#endif //_MSC_VER
//gen-1
