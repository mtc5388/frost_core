function [data, info] = droneTaskControlRequest
%DroneTaskControl gives an empty data for dji_sdk/DroneTaskControlRequest
% Copyright 2019-2020 The MathWorks, Inc.
%#codegen
data = struct();
data.MessageType = 'dji_sdk/DroneTaskControlRequest';
[data.TASKGOHOME, info.TASKGOHOME] = ros.internal.ros.messages.ros.default_type('uint8',1, 1);
[data.TASKTAKEOFF, info.TASKTAKEOFF] = ros.internal.ros.messages.ros.default_type('uint8',1, 4);
[data.TASKLAND, info.TASKLAND] = ros.internal.ros.messages.ros.default_type('uint8',1, 6);
[data.Task, info.Task] = ros.internal.ros.messages.ros.default_type('uint8',1);
info.MessageType = 'dji_sdk/DroneTaskControlRequest';
info.constant = 0;
info.default = 0;
info.maxstrlen = NaN;
info.MaxLen = 1;
info.MinLen = 1;
info.MatPath = cell(1,4);
info.MatPath{1} = 'TASK_GOHOME';
info.MatPath{2} = 'TASK_TAKEOFF';
info.MatPath{3} = 'TASK_LAND';
info.MatPath{4} = 'task';
