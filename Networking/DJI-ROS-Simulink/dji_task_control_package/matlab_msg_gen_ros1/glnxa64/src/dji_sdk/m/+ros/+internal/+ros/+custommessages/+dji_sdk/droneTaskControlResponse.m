function [data, info] = droneTaskControlResponse
%DroneTaskControl gives an empty data for dji_sdk/DroneTaskControlResponse
% Copyright 2019-2020 The MathWorks, Inc.
%#codegen
data = struct();
data.MessageType = 'dji_sdk/DroneTaskControlResponse';
[data.Result, info.Result] = ros.internal.ros.messages.ros.default_type('logical',1);
[data.CmdSet, info.CmdSet] = ros.internal.ros.messages.ros.default_type('uint8',1);
[data.CmdId, info.CmdId] = ros.internal.ros.messages.ros.default_type('uint8',1);
[data.AckData, info.AckData] = ros.internal.ros.messages.ros.default_type('uint32',1);
info.MessageType = 'dji_sdk/DroneTaskControlResponse';
info.constant = 0;
info.default = 0;
info.maxstrlen = NaN;
info.MaxLen = 1;
info.MinLen = 1;
info.MatPath = cell(1,4);
info.MatPath{1} = 'result';
info.MatPath{2} = 'cmd_set';
info.MatPath{3} = 'cmd_id';
info.MatPath{4} = 'ack_data';
