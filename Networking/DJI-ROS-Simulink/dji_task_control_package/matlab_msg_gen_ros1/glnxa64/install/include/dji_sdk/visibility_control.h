#ifndef DJI_SDK__VISIBILITY_CONTROL_H_
#define DJI_SDK__VISIBILITY_CONTROL_H_
#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define DJI_SDK_EXPORT __attribute__ ((dllexport))
    #define DJI_SDK_IMPORT __attribute__ ((dllimport))
  #else
    #define DJI_SDK_EXPORT __declspec(dllexport)
    #define DJI_SDK_IMPORT __declspec(dllimport)
  #endif
  #ifdef DJI_SDK_BUILDING_LIBRARY
    #define DJI_SDK_PUBLIC DJI_SDK_EXPORT
  #else
    #define DJI_SDK_PUBLIC DJI_SDK_IMPORT
  #endif
  #define DJI_SDK_PUBLIC_TYPE DJI_SDK_PUBLIC
  #define DJI_SDK_LOCAL
#else
  #define DJI_SDK_EXPORT __attribute__ ((visibility("default")))
  #define DJI_SDK_IMPORT
  #if __GNUC__ >= 4
    #define DJI_SDK_PUBLIC __attribute__ ((visibility("default")))
    #define DJI_SDK_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define DJI_SDK_PUBLIC
    #define DJI_SDK_LOCAL
  #endif
  #define DJI_SDK_PUBLIC_TYPE
#endif
#endif  // DJI_SDK__VISIBILITY_CONTROL_H_
// Generated 09-Dec-2021 23:09:24
// Copyright 2019-2020 The MathWorks, Inc.
