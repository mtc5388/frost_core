# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "dji_sdk: 0 messages, 1 services")

set(MSG_I_FLAGS "-Istd_msgs:/home/max/sys/ros1/glnxa64/ros1/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(dji_sdk_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/max/Documents/frost_core/Networking/DJI-ROS-Simulink/dji_task_control_package/matlab_msg_gen_ros1/glnxa64/src/dji_sdk/srv/DroneTaskControl.srv" NAME_WE)
add_custom_target(_dji_sdk_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "dji_sdk" "/home/max/Documents/frost_core/Networking/DJI-ROS-Simulink/dji_task_control_package/matlab_msg_gen_ros1/glnxa64/src/dji_sdk/srv/DroneTaskControl.srv" ""
)

#
#  langs = gencpp;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services
_generate_srv_cpp(dji_sdk
  "/home/max/Documents/frost_core/Networking/DJI-ROS-Simulink/dji_task_control_package/matlab_msg_gen_ros1/glnxa64/src/dji_sdk/srv/DroneTaskControl.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/dji_sdk
)

### Generating Module File
_generate_module_cpp(dji_sdk
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/dji_sdk
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(dji_sdk_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(dji_sdk_generate_messages dji_sdk_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/max/Documents/frost_core/Networking/DJI-ROS-Simulink/dji_task_control_package/matlab_msg_gen_ros1/glnxa64/src/dji_sdk/srv/DroneTaskControl.srv" NAME_WE)
add_dependencies(dji_sdk_generate_messages_cpp _dji_sdk_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(dji_sdk_gencpp)
add_dependencies(dji_sdk_gencpp dji_sdk_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS dji_sdk_generate_messages_cpp)

### Section generating for lang: genpy
### Generating Messages

### Generating Services
_generate_srv_py(dji_sdk
  "/home/max/Documents/frost_core/Networking/DJI-ROS-Simulink/dji_task_control_package/matlab_msg_gen_ros1/glnxa64/src/dji_sdk/srv/DroneTaskControl.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/dji_sdk
)

### Generating Module File
_generate_module_py(dji_sdk
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/dji_sdk
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(dji_sdk_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(dji_sdk_generate_messages dji_sdk_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/max/Documents/frost_core/Networking/DJI-ROS-Simulink/dji_task_control_package/matlab_msg_gen_ros1/glnxa64/src/dji_sdk/srv/DroneTaskControl.srv" NAME_WE)
add_dependencies(dji_sdk_generate_messages_py _dji_sdk_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(dji_sdk_genpy)
add_dependencies(dji_sdk_genpy dji_sdk_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS dji_sdk_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/dji_sdk)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/dji_sdk
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(dji_sdk_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/dji_sdk)
  install(CODE "execute_process(COMMAND \"/home/max/.matlab/R2021a/ros1/glnxa64/venv/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/dji_sdk\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/dji_sdk
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(dji_sdk_generate_messages_py std_msgs_generate_messages_py)
endif()
